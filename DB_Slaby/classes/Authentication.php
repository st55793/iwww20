<?php

class Authentication
{
    private $conn = null;
    private static $instance = null;
    private static $identity = null;

    private function __construct()
    {
        if (isset($_SESSION['identity'])) {
            self::$identity = $_SESSION['identity'];
        }
        $this->conn = Connection::getConnection();
    }

    public static function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new Authentication();
        }
        return self::$instance;
    }

    public function GetPassword(){
        $ID = self::getInstance()->getId();
        $query = oci_parse($this->conn, "SELECT heslo FROM uzivatele WHERE id_uzivatele = {$ID}");
        oci_execute($query);
        $result = oci_fetch_array($query);
        return $result;
    }

    public function changePassword($password)
    {
        $id = self::$identity['id'];
        $stmt = oci_parse($this->conn, "UPDATE uzivatele SET heslo = :password WHERE id_uzivatele = :id");
        oci_bind_by_name($stmt, ":id", $id);
        oci_bind_by_name($stmt, ":password", $password);
        oci_execute($stmt);
        return true;
    }

    // funkce pro prihlaseni, overi prihlasovaci udaje a zda-li jsou vyhovujici, tak nastavi session a atribut $identity a vrati true, pokud ne vrati false 
    public function login($login, $password)
    {
        $stmt = oci_parse($this->conn, "SELECT * FROM uzivatele WHERE login = :login");
        oci_bind_by_name($stmt, ":login", $login);
        oci_execute($stmt);
        $result = oci_fetch_array($stmt);

        if ($result == false) {
            return false;
        }

        if (password_verify($password, $result['HESLO'])) {
            $_SESSION['identity'] = array('id' => $result['ID_UZIVATELE'], 'login' => $result['LOGIN'], 'name' => $result['JMENO'], 'surname' => $result['PRIJMENI'], 'role' => $result['ROLE_ID_ROLE'], 'email' => $result['EMAIL']);
            self::$identity = $_SESSION['identity'];
            return true;
        } else {
            return false;
        }
    }

    public function emulation($id_uzivatele)
    {
        $stmt = oci_parse($this->conn, "SELECT * FROM uzivatele WHERE id_uzivatele = :id_uzivatele");
        oci_bind_by_name($stmt, ":id_uzivatele", $id_uzivatele);
        oci_execute($stmt);
        $result = oci_fetch_array($stmt);

        if ($result == false) {
            return false;
        }
        $_SESSION['emulation'] = self::$identity;
        $_SESSION['identity'] = null;
        self::$identity = null;
        $_SESSION['identity'] = array('id' => $result['ID_UZIVATELE'], 'login' => $result['LOGIN'], 'name' => $result['JMENO'], 'surname' => $result['PRIJMENI'], 'role' => $result['ROLE_ID_ROLE'], 'email' => $result['EMAIL']);
        self::$identity = $_SESSION['identity'];
    }

    public function cancelEmulation()
    {
        $_SESSION['identity'] = $_SESSION['emulation'];
        $_SESSION['emulation'] = null;
        self::$identity = $_SESSION['identity'];
    }

    // funkce na registraci, ulozi uzivatele do databaze, pokud je vyvolana vyjimka (v tomto pripade osetreni zadani jiz existujiciho prihlasovaciho jmena - narazeni na unikatni klic)
    // vrati true pri uspesne registraci
    public function register($name, $surname, $login, $password, $email)
    {
        $hashed_password = password_hash($password, PASSWORD_DEFAULT);
        $stmt = oci_parse($this->conn, "BEGIN REGISTRUJ(:login, :password, :name, :surname, 1, :email); END;");
        oci_bind_by_name($stmt, ":login", $login);
        oci_bind_by_name($stmt, ":password", $hashed_password);
        oci_bind_by_name($stmt, ":name", $name);
        oci_bind_by_name($stmt, ":surname", $surname);
        oci_bind_by_name($stmt, ":email", $email);
        try {
            oci_execute($stmt);
        } catch (PDOException $e) {
            return false;
        }
        return true;
    }

    public function logout()
    {
        unset($_SESSION['identity']);
        self::$identity = null;
    }

    public function hasIdentity()
    {
        return self::$identity ? true : false;
    }
    public function setIdentity($identita)
    {
        self::$identity = $identita;
    }

    public function getIdentity()
    {
        return self::$identity ? self::$identity : null;
    }

    public function getId()
    {
        return self::$identity['id'] ? self::$identity['id'] : null;
    }

    public function getLogin()
    {
        return self::$identity['login'] ? self::$identity['login'] : null;
    }

    public function isAdmin()
    {
        return self::$identity['role'] == 5 ? true : false;
    }
    public function isMentor()
    {
        return self::$identity['role'] == 2 ? true : false;
    }

    public function changeUser($login, $email)
    {
        $id = self::$identity['id'];
        $stmt = oci_parse($this->conn, "UPDATE uzivatele SET login = :login, email = :email WHERE id_uzivatele = :id");
        oci_bind_by_name($stmt, ":id", $id);
        oci_bind_by_name($stmt, ":login", $login);
        oci_bind_by_name($stmt, ":email", $email);
        try {
            oci_execute($stmt);
            $_SESSION['identity']['login'] = $login;
            $_SESSION['identity']['email'] = $email;
            self::$identity = $_SESSION['identity'];
        } catch (PDOException $e) {
            return false;
        }
        return true;
    }

    public function getImage($id)
    {
        $stmt = oci_parse($this->conn, "SELECT obrazek FROM profilovy_obrazek WHERE UZIVATELE_ID_UZIVATELE = :id");
        oci_bind_by_name($stmt, ":id", $id);
        oci_execute($stmt);
        oci_fetch_all($stmt, $res, null, null, OCI_FETCHSTATEMENT_BY_ROW);
        if (sizeof($res) == 0) {
            return null;
        }
        return $res[0];
    }
}

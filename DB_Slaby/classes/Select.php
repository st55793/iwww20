<?php

class Select
{
    private $conn = null;
    private static $instance = null;

    public function __construct()
    {
        $this->conn = Connection::getConnection();
    }

    public static function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new Select();
        }
        return self::$instance;
    }

    //OK

    public function getRole($id)
    {
        $stmt = oci_parse($this->conn, "SELECT * FROM P_ROLE WHERE id_role = :id");
        oci_bind_by_name($stmt, ":id", $id);
        oci_execute($stmt);
        $stmt = oci_fetch_array($stmt);
        return $stmt['NAZEV'];
    }

    //OK

    public function getAllUsers()
    {
        $stmt = oci_parse($this->conn, "SELECT * FROM P_UZIVATELE ORDER BY PRIJMENI ASC");
        try {
            oci_execute($stmt);
            oci_fetch_all($stmt, $res, null, null, OCI_FETCHSTATEMENT_BY_COLUMN);
            return $res;
        } catch (PDOException $e) {
            return null;
        }
    }

    //OK

    public function getRoleM()
    {
        $stmt = oci_parse($this->conn, "SELECT * FROM P_ROLE");
        oci_execute($stmt);
        oci_fetch_all($stmt, $res);
        return $res;
    }

    //OK

    public function getPredmety()
    {
        $stmt = oci_parse($this->conn, "SELECT * from p_predmety");
        try {
            oci_execute($stmt);
            oci_fetch_all($stmt, $res, null, null, OCI_FETCHSTATEMENT_BY_ROW);
            return $res;
        } catch (PDOException $e) {
            return null;
        }
    }

    //OK

    public function getKontrola($id_predmet)
    {
        $stmt = oci_parse($this->conn, "SELECT * FROM P_UCITELE WHERE ID_UZIVATELE = :id AND ID_PREDMET = :id_predmet");
        $id = Authentication::getInstance()->getId();
        oci_bind_by_name($stmt, ":id", $id);
        oci_bind_by_name($stmt, ":id_predmet", $id_predmet);
        try {
            oci_execute($stmt);
            oci_fetch_all($stmt, $res, null, null, OCI_FETCHSTATEMENT_BY_ROW);
            if (empty($res)) {
                return false;
            } else {
                return true;
            }
        } catch (PDOException $e) {
            return false;
        }
    }

    //OK

    public function getPredmet($id)
    {
        $stmt = oci_parse($this->conn, "SELECT * from P_PREDMETY WHERE id_predmet = :id");
        oci_bind_by_name($stmt, ":id", $id);
        try {
            oci_execute($stmt);
            oci_fetch_all($stmt, $res, null, null, OCI_FETCHSTATEMENT_BY_ROW);
            return $res;
        } catch (PDOException $e) {
            return null;
        }
    }

    //OK

    public function getGarantPrednasejici($id_predmet, $id_role){
        $stmt = oci_parse($this->conn, "SELECT * from P_UCITELE WHERE id_predmet = :id AND id_role = :id_role");
        oci_bind_by_name($stmt, ":id", $id_predmet);
        oci_bind_by_name($stmt, ":id_role", $id_role);
        try {
            oci_execute($stmt);
            oci_fetch_all($stmt, $res, null, null, OCI_FETCHSTATEMENT_BY_ROW);
            return $res;
        } catch (PDOException $e) {
            return null;
        }
    }

    //OK

    public function getUcitele()
    {
        $stmt = oci_parse($this->conn, "SELECT * FROM P_UZIVATELE WHERE id_role = 2 ORDER BY id_uzivatele ASC");
        try {
            oci_execute($stmt);
            oci_fetch_all($stmt, $res, null, null, OCI_FETCHSTATEMENT_BY_ROW);
            return $res;
        } catch (PDOException $e) {
            return null;
        }
    }

    //OK
    public function getKontakty(){
        $id_uzivatele = Authentication::getInstance()->getId();
        $stmt = oci_parse($this->conn, "SELECT * FROM P_KONTAKTY WHERE id_1 = {$id_uzivatele} OR id_2 = {$id_uzivatele} ORDER BY prijmeni_1 ASC");
        try {
            oci_execute($stmt);
            oci_fetch_all($stmt, $res, null, null, OCI_FETCHSTATEMENT_BY_ROW);
            return $res;
        } catch (PDOException $e) {
            return null;
        }
    }

    public function getKontakt($id_kontakt){
        $id_uzivatele = Authentication::getInstance()->getId();
        $stmt = oci_parse($this->conn, "SELECT * FROM P_KONTAKTY WHERE id_kontakt = :id_kontakt");
        try {
            oci_bind_by_name($stmt, ":id_kontakt", $id_kontakt);
            oci_execute($stmt);
            oci_fetch_all($stmt, $res, null, null, OCI_FETCHSTATEMENT_BY_ROW);
            return $res[0];
        } catch (PDOException $e) {
            return null;
        }
    }

    //OK
    public function getZpravy($id_kontakt){
        $stmt = oci_parse($this->conn, "SELECT * FROM P_ZPRAVY WHERE id_kontakt = :id_kontakt");
        try {
            oci_bind_by_name($stmt, ":id_kontakt", $id_kontakt);
            oci_execute($stmt);
            oci_fetch_all($stmt, $res, null, null, OCI_FETCHSTATEMENT_BY_ROW);
            return $res;
        } catch (PDOException $e) {
            return null;
        }
    }

    //OK
    public function getZprava($id_zprava){
        $id_uzivatele = Authentication::getInstance()->getId();
        $stmt = oci_parse($this->conn, "SELECT * FROM P_ZPRAVY WHERE id_zprava = :id_zprava");
        try {
            oci_bind_by_name($stmt, ":id_zprava", $id_zprava);
            oci_execute($stmt);
            oci_fetch_all($stmt, $res, null, null, OCI_FETCHSTATEMENT_BY_ROW);
            return $res[0];
        } catch (PDOException $e) {
            return null;
        }
    }

    //OK
    public function getUzivatel($id)
    {
        $stmt = oci_parse($this->conn, "SELECT * FROM P_UZIVATELE WHERE id_uzivatele = :id");
        oci_bind_by_name($stmt, ":id", $id);
        oci_execute($stmt);
        oci_fetch_all($stmt, $res, null, null, OCI_FETCHSTATEMENT_BY_ROW);
        return $res[0];
    }

    //OK

    public function getMaterialy($nazev, $predmet, $prijmeni)
    {
        $stmt = oci_parse($this->conn, "SELECT * FROM P_MATERIALY WHERE nazev LIKE :nazev AND nazev_predmetu LIKE :predmet AND prijmeni LIKE :prijmeni ORDER BY NAZEV, NAZEV_PREDMETU ASC");
        try {
            oci_bind_by_name($stmt, ":nazev", $nazev);
            oci_bind_by_name($stmt, ":predmet", $predmet);
            oci_bind_by_name($stmt, ":prijmeni", $prijmeni);
            oci_execute($stmt);
            oci_fetch_all($stmt, $res, null, null, OCI_FETCHSTATEMENT_BY_ROW);
            return $res;
        } catch (PDOException $e) {
            return null;
        }
    }

    //OK
    public function getPredmetyMaterialy()
    {
        if (Authentication::getInstance()->isAdmin()) {
            $stmt = oci_parse($this->conn, "SELECT DISTINCT ID_PREDMET, NAZEV_PREDMETU FROM P_UCITELE ORDER BY NAZEV_PREDMETU ASC");
        }else{
            $id = Authentication::getInstance()->getId();
            $stmt = oci_parse($this->conn, "SELECT DISTINCT ID_PREDMET, NAZEV_PREDMETU FROM P_UCITELE WHERE ID_UZIVATELE = :id ORDER BY NAZEV_PREDMETU ASC");
            oci_bind_by_name($stmt, ":id", $id);
        }
        try {
            oci_execute($stmt);
            oci_fetch_all($stmt, $res, null, null, OCI_FETCHSTATEMENT_BY_ROW);
            return $res;
        } catch (PDOException $e) {
            return null;
        }
    }

    //OK
    public function getMaterial($id)
    {
        $stmt = oci_parse($this->conn, "SELECT * FROM P_MATERIALY WHERE id_material = :id");
        oci_bind_by_name($stmt, ":id", $id);
        oci_execute($stmt);
        oci_fetch_all($stmt, $res, null, null, OCI_FETCHSTATEMENT_BY_ROW);
        return $res[0];
    }

    //OK
    public function getKategorie()
    {
        $stmt = oci_parse($this->conn, "SELECT * FROM P_KATEGORIE");
        oci_execute($stmt);
        oci_fetch_all($stmt, $res, null, null, OCI_FETCHSTATEMENT_BY_ROW);
        return $res;
    }

    //OK
    public function getKomentare($id_material)
    {
        $stmt = oci_parse($this->conn, "SELECT * FROM P_KOMENTARE WHERE ID_MATERIAL = :id ORDER BY ID_KOMENTAR");
        oci_bind_by_name($stmt, ":id", $id_material);
        oci_execute($stmt);
        oci_fetch_all($stmt, $res, null, null, OCI_FETCHSTATEMENT_BY_ROW);
        return $res;
    }

    //OK
    public function getKvizy($id_material)
    {
        $stmt = oci_parse($this->conn, "SELECT * FROM P_KVIZY WHERE ID_MATERIAL = :id");
        oci_bind_by_name($stmt, ":id", $id_material);
        oci_execute($stmt);
        oci_fetch_all($stmt, $res, null, null, OCI_FETCHSTATEMENT_BY_ROW);
        return $res;
    }


    //OK
    public function getMatKat($id)
    {
        $stmt = oci_parse($this->conn, "SELECT * FROM P_MAT_KAT WHERE ID_MATERIAL = :id");
        oci_bind_by_name($stmt, ":id", $id);
        oci_execute($stmt);
        oci_fetch_all($stmt, $res, null, null, OCI_FETCHSTATEMENT_BY_ROW);
        return $res;
    }

    //ok
    public function getKomentar($id_komentar)
    {
        $stmt = oci_parse($this->conn, "SELECT * FROM P_KOMENTARE WHERE ID_KOMENTAR = :id");
        oci_bind_by_name($stmt, ":id", $id_komentar);
        oci_execute($stmt);
        oci_fetch_all($stmt, $res, null, null, OCI_FETCHSTATEMENT_BY_ROW);
        return $res[0];
    }

    //OK
    public function getKviz($id_kviz)
    {
        $stmt = oci_parse($this->conn, "SELECT * FROM P_KVIZY WHERE id_kviz = :id");
        oci_bind_by_name($stmt, ":id", $id_kviz);
        oci_execute($stmt);
        oci_fetch_all($stmt, $res, null, null, OCI_FETCHSTATEMENT_BY_ROW);
        return $res[0];
    }

    //OK
    public function getOtazky($id_kviz)
    {
        $stmt = oci_parse($this->conn, "SELECT * FROM P_OTAZKY WHERE ID_KVIZ = :id");
        oci_bind_by_name($stmt, ":id", $id_kviz);
        oci_execute($stmt);
        oci_fetch_all($stmt, $res, null, null, OCI_FETCHSTATEMENT_BY_ROW);
        return $res;
    }

    //OK
    Public function getTypyOtazka()
    {
        $stmt = oci_parse($this->conn, "SELECT * FROM P_TYPY");
        oci_execute($stmt);
        oci_fetch_all($stmt, $res, null, null, OCI_FETCHSTATEMENT_BY_ROW);
        return $res;
    }

    //OK
    public function getOtazka($id)
    {
        $stmt = oci_parse($this->conn, "SELECT * FROM P_otazky WHERE ID_OTAZKA = :id");
        oci_bind_by_name($stmt, ":id", $id);
        oci_execute($stmt);
        oci_fetch_all($stmt, $res, null, null, OCI_FETCHSTATEMENT_BY_ROW);
        return $res[0];
    }

    //OK
    public function getOdpoved($id_odpoved)
    {
        $stmt = oci_parse($this->conn, "SELECT * FROM P_ODPOVEDI WHERE ID_ODPOVED = :id");
        oci_bind_by_name($stmt, ":id", $id_odpoved);
        oci_execute($stmt);
        oci_fetch_all($stmt, $res, null, null, OCI_FETCHSTATEMENT_BY_ROW);
        return $res[0];
    }

    //OK
    public function getOdpovedi($id_otazka)
    {
        $stmt = oci_parse($this->conn, "SELECT * FROM P_ODPOVEDI WHERE ID_OTAZKA = :id ORDER BY ID_ODPOVED");
        oci_bind_by_name($stmt, ":id", $id_otazka);
        oci_execute($stmt);
        oci_fetch_all($stmt, $res, null, null, OCI_FETCHSTATEMENT_BY_ROW);
        return $res;
    }

    //OK
    public function getSlova()
    {
        $stmt = oci_parse($this->conn, "SELECT * FROM P_NEVHODNA_SLOVA ORDER BY slovo ASC");
        oci_execute($stmt);
        oci_fetch_all($stmt, $res, null, null, OCI_FETCHSTATEMENT_BY_ROW);
        return $res;
    }

    //OK
    public function getPocetPokusu($id_kviz)
    {
        $id = Authentication::getInstance()->getId();
        $stmt = oci_parse($this->conn, "SELECT COUNT(*) AS POCET FROM P_VYSLEDKY WHERE id_kviz = :id_kviz AND id_uzivatele = {$id}");
        oci_bind_by_name($stmt, ":id_kviz", $id_kviz);
        oci_execute($stmt);
        oci_fetch_all($stmt, $res, null, null, OCI_FETCHSTATEMENT_BY_ROW);
        return $res;
    }

    //OK
    public function getPocetOtazek($id_kviz)
    {
        $stmt = oci_parse($this->conn, "SELECT COUNT(*) AS POCET FROM P_OTAZKY WHERE id_kviz = :id_kviz");
        oci_bind_by_name($stmt, ":id_kviz", $id_kviz);
        oci_execute($stmt);
        oci_fetch_all($stmt, $res, null, null, OCI_FETCHSTATEMENT_BY_ROW);
        return $res;
    }

    //OK
    public function getMaxIdOtazky()
    {
        $stmt = oci_parse($this->conn, "select MAX(id_otazka) AS max from P_OTAZKY");
        oci_execute($stmt);
        oci_fetch_all($stmt, $res, null, null, OCI_FETCHSTATEMENT_BY_ROW);
        return $res[0]['MAX'];
    }

    //OK
    public function getvysledky($nazev_predmetu, $nazev_kvizu, $prijmeni)
    {
        $id = Authentication::getInstance()->getId();
        if(Authentication::getInstance()->isAdmin()){
            $stmt = oci_parse($this->conn, "SELECT * FROM P_VYSLEDKY WHERE nazev_predmetu LIKE :nazev_predmetu AND nazev_kvizu LIKE :nazev_kvizu AND prijmeni LIKE :prijmeni");
        }else if(Authentication::getInstance()->isMentor()){
            $stmt = oci_parse($this->conn, "SELECT * FROM P_VYSLEDKY WHERE nazev_predmetu LIKE :nazev_predmetu AND nazev_kvizu LIKE :nazev_kvizu 
            AND prijmeni LIKE :prijmeni AND ID_PREDMET IN (SELECT DISTINCT ID_PREDMET FROM P_UCITELE WHERE ID_UZIVATELE = :id)");
            oci_bind_by_name($stmt, ":id", $id);
        }else{
            $stmt = oci_parse($this->conn, "SELECT * FROM P_VYSLEDKY WHERE nazev_predmetu LIKE :nazev_predmetu AND nazev_kvizu LIKE :nazev_kvizu AND prijmeni LIKE :prijmeni AND id_uzivatele = :id");
            oci_bind_by_name($stmt, ":id", $id);
        }

        oci_bind_by_name($stmt, ":nazev_predmetu", $nazev_predmetu);
        oci_bind_by_name($stmt, ":nazev_kvizu", $nazev_kvizu);
        oci_bind_by_name($stmt, ":prijmeni", $prijmeni);
        oci_execute($stmt);
        oci_fetch_all($stmt, $res, null, null, OCI_FETCHSTATEMENT_BY_ROW);
        return $res;
    }
}

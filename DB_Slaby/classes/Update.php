<?php
class Update
{
    private $conn = null;
    private static $instance = null;

    public function __construct()
    {
        $this->conn = Connection::getConnection();
    }

    public static function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new Update();
        }
        return self::$instance;
    }

    //ok

    public function updateRole($id, $id_role)
    {
        $stmt = oci_parse($this->conn, "UPDATE uzivatele SET ROLE_ID_ROLE = :id_role WHERE id_uzivatele = :id");
        oci_bind_by_name($stmt, ":id", $id);
        oci_bind_by_name($stmt, ":id_role", $id_role);
        try {
            oci_execute($stmt);
        } catch (PDOException $e) {
            return false;
        }
        return true;
    }

    //OK

    public function updatePredmet($id, $nazev, $popis)
    {
        $stmt = oci_parse($this->conn, "UPDATE PREDMET SET NAZEV = :nazev, POPIS = :popis WHERE ID_PREDMET = :id");
        oci_bind_by_name($stmt, ":id", $id);
        oci_bind_by_name($stmt, ":nazev", $nazev);
        oci_bind_by_name($stmt, ":popis", $popis);
        try {
            oci_execute($stmt);
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }

    //OK
    public function updateZprava($id, $text)
    {
        $stmt = oci_parse($this->conn, "UPDATE ZPRAVA SET text = :text WHERE ID_ZPRAVA = :id");
        oci_bind_by_name($stmt, ":id", $id);
        oci_bind_by_name($stmt, ":text", $text);
        oci_execute($stmt);
        return true;
    }

    //OK
    public function updateMaterial($nazev, $popis, $id_predmet, $id_material)
    {

        $stmt = oci_parse($this->conn, "UPDATE STUDIJNI_MATERIAL SET nazev = :nazev, popis = :popis, datum_zmeny = CURRENT_TIMESTAMP, id_predmet = :id_predmet WHERE ID_MATERIAL = :id_material");
        oci_bind_by_name($stmt, ":nazev", $nazev);
        oci_bind_by_name($stmt, ":popis", $popis);
        oci_bind_by_name($stmt, ":id_predmet", $id_predmet);
        oci_bind_by_name($stmt, ":id_material", $id_material);
        oci_execute($stmt);
    }

    //OK
    public function updateKomentar($id, $text)
    {
        $stmt = oci_parse($this->conn, "UPDATE KOMENTARE SET text = :text WHERE ID_KOMENTAR = :id");
        oci_bind_by_name($stmt, ":id", $id);
        oci_bind_by_name($stmt, ":text", $text);
        oci_execute($stmt);
        return true;
    }
    
    //OK
    public function updateKviz($id, $nazev, $popis, $pokusu)
    {
        $stmt = oci_parse($this->conn, "UPDATE KVIZ SET NAZEV = :nazev, POPIS = :popis, pocet_pokusu = :pocet_pokusu WHERE ID_KVIZ = :id");
        oci_bind_by_name($stmt, ":id", $id);
        oci_bind_by_name($stmt, ":nazev", $nazev);
        oci_bind_by_name($stmt, ":popis", $popis);
        oci_bind_by_name($stmt, ":pocet_pokusu", $pokusu);
        try {
            oci_execute($stmt);
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }

    //OK
    public function updatePocetSpravnych($pocet, $id_otazky)
    {
        $stmt = oci_parse($this->conn, "UPDATE OTAZKA SET POCET_SPRAVNYCH = :pocet WHERE ID_OTAZKA = {$id_otazky}");
        oci_bind_by_name($stmt, ":pocet", $pocet);
        oci_execute($stmt);
        return true;
    }

    //OK
    public function updateOtazka($id, $otazka, $body, $id_typ)
    {
        $stmt = oci_parse($this->conn, "UPDATE otazka SET otazka = :otazka, body = :body, id_typ = :id_typ WHERE id_otazka = :id");
        oci_bind_by_name($stmt, ":id", $id);
        oci_bind_by_name($stmt, ":otazka", $otazka);
        oci_bind_by_name($stmt, ":body", $body);
        oci_bind_by_name($stmt, ":id_typ", $id_typ);
        oci_execute($stmt);
        return true;
    }
}

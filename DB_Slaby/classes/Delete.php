<?php

class Delete
{
    private $conn = null;
    private static $instance = null;

    public function __construct()
    {
        $this->conn = Connection::getConnection();
    }

    public static function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new Delete();
        }
        return self::$instance;
    }

    //OK
    public function deleteKontakt($id_kontakt)
    {
        $id_kontakt = intval($id_kontakt);
        $stmt = oci_parse($this->conn, "BEGIN SMAZ_KONTAKT(:id_kontakt); END;");
        oci_bind_by_name($stmt, ":id_kontakt", $id_kontakt);
        oci_execute($stmt);
    }

    //OK
    public function deleteOpravneni($id_predmet)
    {
        $id = intval($id_predmet);
        $stmt = oci_parse($this->conn, "DELETE FROM OPRAVNENI WHERE PREDMET_ID_PREDMET = :id_predmet");
        oci_bind_by_name($stmt, ":id_predmet", $id);
        oci_execute($stmt);
    }

    //OK
    public function deleteZprava($id_zprava)
    {
        $stmt = oci_parse($this->conn, "DELETE FROM ZPRAVA WHERE id_zprava = :id_zprava");
        oci_bind_by_name($stmt, ":id_zprava", $id_zprava);
        oci_execute($stmt);
    }

    //OK
    public function deleteMatKat($id_material)
    {
        $stmt = oci_parse($this->conn, "DELETE FROM MATERIAL_KATEGORIE WHERE ID_MATERIAL = :id");
        oci_bind_by_name($stmt, ":id", $id_material);
        oci_execute($stmt);
    }

    //OK
    public function deleteKomentar($id_komentar)
    {
        $stmt = oci_parse($this->conn, "DELETE FROM KOMENTARE WHERE id_komentar = :id");
        oci_bind_by_name($stmt, ":id", $id_komentar);
        oci_execute($stmt);
    }

    //OK
    public function deleteOdpoved($id_otazka)
    {
        $stmt = oci_parse($this->conn, "DELETE FROM ODPOVED WHERE id_otazka = :id");
        oci_bind_by_name($stmt, ":id", $id_otazka);
        oci_execute($stmt);
    }

    //OK
    public function deleteKviz($id_kviz)
    {
        $stmt = oci_parse($this->conn, "BEGIN SMAZ_KVIZ(:id_kviz); END;");
        oci_bind_by_name($stmt, ":id_kviz", $id_kviz);
        oci_execute($stmt);
    }

    //OK
    public function deleteOtazka($id_otazka)
    {
        $stmt = oci_parse($this->conn, "BEGIN SMAZ_OTAZKU(:id_otazka); END;");
        oci_bind_by_name($stmt, ":id_otazka", $id_otazka);
        oci_execute($stmt);
    }

    //OK
    public function deleteKomentare($id_material)
    {
        $stmt = oci_parse($this->conn, "DELETE FROM KOMENTARE WHERE ID_MATERIAL = :id_material");
        oci_bind_by_name($stmt, ":id_material", $id_material);
        oci_execute($stmt);
    }

    //OK
    public function deleteMaterial($id_material)
    {
        $stmt = oci_parse($this->conn, "BEGIN SMAZ_MATERIAL(:id_material); END;");
        oci_bind_by_name($stmt, ":id_material", $id_material);
        oci_execute($stmt);
    }

    //OK
    public function deletePredmet($id_predmet)
    {
        $stmt = oci_parse($this->conn, "BEGIN SMAZ_PREDMET(:id_predmet); END;");
        oci_bind_by_name($stmt, ":id_predmet", $id_predmet);
        oci_execute($stmt);
    }

    //OK
    public function deleteSlovo($id_slovo)
    {
        $stmt = oci_parse($this->conn, "DELETE FROM NEVHODNA_SLOVA WHERE ID_slovo = :id_slovo");
        oci_bind_by_name($stmt, ":id_slovo", $id_slovo);
        oci_execute($stmt);
    }
    public function deleteVysledek($id_vysledek)
    {
        $stmt = oci_parse($this->conn, "DELETE FROM VYSLEDKY WHERE id_vysledek = :id_vysledek");
        oci_bind_by_name($stmt, ":id_vysledek", $id_vysledek);
        oci_execute($stmt);
    }
}

<?php
class Insert
{
    private $conn = null;
    private static $instance = null;

    public function __construct()
    {
        $this->conn = Connection::getConnection();
    }

    public static function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new Insert();
        }
        return self::$instance;
    }

    //OK 

    public function InsertOpravnění($id_uzivatele, $id_role, $id_predmet)
    {
        $id_uzivatel = Authentication::getInstance()->getId();
        $stmt = oci_parse($this->conn, "INSERT INTO OPRAVNENI VALUES (OPRAVNENI_SEQ.NEXTVAL, :id_role, :id_uzivatele, :id_predmet)");
        oci_bind_by_name($stmt, ":id_role", $id_role);
        oci_bind_by_name($stmt, ":id_uzivatele", $id_uzivatele);
        oci_bind_by_name($stmt, ":id_predmet", $id_predmet);
        oci_execute($stmt);
    }

    //OK
    public function InsertPredmet($nazev, $popis)
    {
        $stmt = oci_parse($this->conn, "INSERT INTO PREDMET VALUES (PREDMET_SEQ.NEXTVAL, :nazev, :popis)");
        oci_bind_by_name($stmt, ":nazev", $nazev);
        oci_bind_by_name($stmt, ":popis", $popis);
        oci_execute($stmt);
    }

    //OK
    public function InsertKontakt($id_kontaktu)
    {
        $ID = Authentication::getInstance()->getId();
        $stmt = oci_parse($this->conn, "INSERT INTO KONTAKT VALUES (KONTAKT_SEQ.NEXTVAL, :id_kontaktu, {$ID})");
        oci_bind_by_name($stmt, ":id_kontaktu", $id_kontaktu);
        oci_execute($stmt);
    }
    
    //OK
    public function InsertZprava($text, $id_prijemce, $id_kontakt)
    {
        $ID = Authentication::getInstance()->getId();
        $stmt = oci_parse($this->conn, "INSERT INTO zprava VALUES (ZPRAVA_SEQ.NEXTVAL, :text, CURRENT_TIMESTAMP, {$ID}, :id_prijemce, :id_kontakt)");
        oci_bind_by_name($stmt, ":text", $text);
        oci_bind_by_name($stmt, ":id_prijemce", $id_prijemce);
        oci_bind_by_name($stmt, ":id_kontakt", $id_kontakt);
        oci_execute($stmt);
    }

    //OK
    public function InsertMaterial($nazev, $popis, $id_predmet)
    {
        $ID = Authentication::getInstance()->getId();
        $stmt = oci_parse($this->conn, "INSERT INTO STUDIJNI_MATERIAL VALUES (MATERIAL_SEQ.NEXTVAL, :nazev, :popis, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, :id_predmet, {$ID})");
        oci_bind_by_name($stmt, ":nazev", $nazev);
        oci_bind_by_name($stmt, ":popis", $popis);
        oci_bind_by_name($stmt, ":id_predmet", $id_predmet);
        oci_execute($stmt);
    }

    //OK
    public function InsertMatKat($id_material, $id_kategorie)
    {
        $stmt = oci_parse($this->conn, "INSERT INTO MATERIAL_KATEGORIE (ID_MATERIAL, ID_KATEGORIE) VALUES (:id_material, :id_kategorie)");
        oci_bind_by_name($stmt, ":id_material", $id_material);
        oci_bind_by_name($stmt, ":id_kategorie", $id_kategorie);
        oci_execute($stmt);
    }

    //OK
    public function InsertKomentar($text, $id_material)
    {
        $id_uzivatele = intval(Authentication::getInstance()->getId());

        $stmt = oci_parse($this->conn, "INSERT INTO KOMENTARE VALUES (KOMENTAR_SEQ.NEXTVAL, :text, :id_material, {$id_uzivatele})");
        oci_bind_by_name($stmt, ":text", $text);
        oci_bind_by_name($stmt, ":id_material", $id_material);
        oci_execute($stmt);
    }

    //OK
    public function InsertKviz($nazev, $popis, $id_material, $pokus)
    {
        $stmt = oci_parse($this->conn, "INSERT INTO KVIZ VALUES (KVIZ_SEQ.NEXTVAL, :nazev, :popis, :id_material, CURRENT_TIMESTAMP, :pokus)");
        oci_bind_by_name($stmt, ":nazev", $nazev);
        oci_bind_by_name($stmt, ":popis", $popis);
        oci_bind_by_name($stmt, ":id_material", $id_material);
        oci_bind_by_name($stmt, ":pokus", $pokus);
        oci_execute($stmt);
    }

    //OK
    public function InsertOtazka($otazka, $body, $id_kviz, $id_typ)
    {
        
        $stmt = oci_parse($this->conn, "INSERT INTO otazka VALUES (OTAZKA_SEQ.NEXTVAL, :otazka, :body, :id_kviz, :id_typ, 0) returning ID_OTAZKA into :id_res");
        oci_bind_by_name($stmt, ":otazka", $otazka);
        oci_bind_by_name($stmt, ":body", $body);
        oci_bind_by_name($stmt, ":id_kviz", $id_kviz);
        oci_bind_by_name($stmt, ":id_typ", $id_typ);
        oci_bind_by_name($stmt, ":id_res", $res);
        oci_execute($stmt);
        return $res;
    }

    //OK
    public function InsertOdpoved($odpoved, $priznak, $id_otazka)
    {
        $stmt = oci_parse($this->conn, "INSERT INTO odpoved VALUES (ODPOVED_SEQ.NEXTVAL, :odpoved, :priznak, :id_otazka)");   
        oci_bind_by_name($stmt, ":odpoved", $odpoved);
        oci_bind_by_name($stmt, ":priznak", $priznak);
        oci_bind_by_name($stmt, ":id_otazka", $id_otazka);
        oci_execute($stmt);
    }

    //OK
    public function InsertSlovo($slovo)
    {
        $stmt = oci_parse($this->conn, "INSERT INTO NEVHODNA_SLOVA VALUES (SLOVA_SEQ.NEXTVAL, :slovo)");   
        oci_bind_by_name($stmt, ":slovo", $slovo);
        oci_execute($stmt);
    }

    //OK
    public function InsertVysledek($vysledek, $spravne, $castecne, $spatne, $id_kviz)
    {
        $id = Authentication::getInstance()->getId();
        $stmt = oci_parse($this->conn, "INSERT INTO vysledky VALUES (VYSLEDKY_SEQ.NEXTVAL, :vysledek, :spravne, :castecne, :spatne, :id_uzivatele, :id_kviz)");
        oci_bind_by_name($stmt, ":vysledek", $vysledek);
        oci_bind_by_name($stmt, ":spravne", $spravne);
        oci_bind_by_name($stmt, ":castecne", $castecne);
        oci_bind_by_name($stmt, ":spatne", $spatne);
        oci_bind_by_name($stmt, ":id_uzivatele", $id);
        oci_bind_by_name($stmt, ":id_kviz", $id_kviz);
        oci_execute($stmt);
    }
}
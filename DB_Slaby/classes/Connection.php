<?php

class Connection
{
    private static $instance = null;

    private function __construct()
    {
    }
    
    public static function getConnection()
    {
        if (self::$instance == null) {
            $conn = oci_connect(DB_USER, DB_PASSWORD, DB_SERVER, 'AL32UTF8');
            self::$instance = $conn;
        }
        return self::$instance;
    }
}

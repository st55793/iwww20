<?php if (Authentication::getInstance()->hasIdentity()) :
    $identita = Authentication::getInstance()->getIdentity();
    if (isset($_POST['upravitObrazek'])) {

        if (empty(trim($_POST['login'])) || empty(trim($_POST['email']))) {
            echo "Login ani email nesmí být prázdný";
        } else {
            Authentication::getInstance()->changeUser($_POST['login'], $_POST['email']);
            exit(header('Location: ' . BASE_URL . '?page=profile'));
        }

        if (is_uploaded_file($_FILES['image']['tmp_name'])) {
            $img_upload = $_FILES['image']['tmp_name'];
            $image = null;
            if ($img_upload) {
                $info = pathinfo($_FILES['image']['name']);
                $extension = $info['extension'];
                $image = file_get_contents($img_upload);
            }
            if (!$image) {
                $warning = "Nebyl vybrán obrázek";
            } else {
                $conn = Connection::getConnection();
                $ID = intval(Authentication::getInstance()->getId());
                $query = oci_parse($conn, "INSERT INTO profilovy_obrazek VALUES(PROFILOVY_OBRAZEK_SEQ.NEXTVAL, empty_blob(), '{$extension}', CURRENT_TIMESTAMP, {$ID}) RETURNING obrazek INTO :image");
                $blob = oci_new_descriptor($conn, OCI_D_LOB);
                oci_bind_by_name($query, ":image", $blob, -1, OCI_B_BLOB);
                oci_execute($query, OCI_DEFAULT);
                if (!$blob->save($image)) {
                    oci_rollback($conn);
                } else {
                    oci_commit($conn);
                    $success = "Obrázek byl nahrán";
                }
                oci_free_statement($query);
                $blob->free();
                if (isset($success)) {
                    exit(header('Location: ' . BASE_URL . '?page=profile'));
                }
            }
        }
    }
    if (isset($_POST['Cancel_emulation'])) {
        Authentication::getInstance()->cancelEmulation();
        exit(header('Location: ' . BASE_URL . '?page=profile'));
    }

    if (isset($_SESSION['emulation'])) {
        echo "<h3>aktuálně jsi emulován a nejsi na svém účtu kliknutím na tlačítko níže se vrátíš na svůj účet</h3>";
?>

        <form method="post" enctype="multipart/form-data">
            <div class="form-submit">
                <input type="submit" name="Cancel_emulation" value="Vypnout emulaci">
            </div>
        </form>
    <?php
    }
    ?>
    <h3>Vítej <?php echo $identita['login'] ?></h3>
    <div>
        <table class="profile">
            <tr>
                <th>ID:</th>
                <th><?php echo $identita['id'] ?></th>
            </tr>
            <tr>
                <th>Login:</th>
                <th><?php echo $identita['login'] ?></th>
            </tr>
            <tr>
                <th>Jméno:</th>
                <th><?php echo $identita['name'] ?></th>
            </tr>
            <tr>
                <th>Příjmení</th>
                <th><?php echo $identita['surname'] ?></th>
            </tr>
            <tr>
                <th>Email</th>
                <th><?php echo $identita['email'] ?></th>
            </tr>
            <tr>
                <th>Role: </th>
                <th><?php echo Select::getInstance()->getRole($identita['role']) ?></th>
            </tr>
            <tr>
                <th>Změna hesla:</th>
                <th><a href="<?php echo BASE_URL . '?page=change_password' ?>">Změna hesla</a></th>
            </tr>
            <tr>
                <th>Kontakty:</th>
                <th><a href="<?php echo BASE_URL . '?page=contacts' ?>">Kontakty</a></th>
            </tr>
            <tr>
                <th>Výsledky kvízů:</th>
                <th><a href="<?php echo BASE_URL . '?page=vysledky' ?>">Vysledky</a></th>
            </tr>
            <?php if (Authentication::getInstance()->isAdmin()) : ?>
                <tr>
                    <th>Nevhodná slova:</th>
                    <th><a href="<?php echo BASE_URL . '?page=nevhodna_slova' ?>">Nevhodná slova</a></th>
                </tr>
                <tr>
                    <th>Emulace: </th>
                    <th><a href="<?php echo BASE_URL . '?page=emulation' ?>">Emulace</a></th>
                </tr>
            <?php endif; ?>
            <tr>
                <th style="padding-right: 50px;">Obrázek: </th>
                <th><?php
                    $actual_image = Authentication::getInstance()->getImage($identita['id']);
                    if ($actual_image == null) :
                        echo "Není nahrán žádný obrázek";
                    else :
                        echo "<img width='100' height='100' src='data:image/jpeg;base64," . base64_encode($actual_image['OBRAZEK']) . "' alt='Profilový obrázek'>";
                    endif;
                    ?></th>
            </tr>
        </table>
        <button onclick="ShowDialog()">Upravit profil</button>
        <div class="card" style="display: none; margin-top: 15px;" id="dialog">
            <div class="card-title">
                <h2>Úprava profilu</h2>
            </div>
            <div class="card-body">
                <form method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label>Přihlašovací jméno:</label>
                        <input type="text" name="login" value="<?php echo $identita['login'] ?>">
                    </div>
                    <div class="form-group">
                        <label>Email:</label>
                        <input type="email" name="email" value="<?php echo $identita['email'] ?>">
                    </div>
                    <div class="form-group">
                        <label>Obrázek:</label>
                        <input type="file" name="image" placeholder="Nahrát obrázek">
                    </div>
                    <div class="form-submit">
                        <input type="submit" name="upravitObrazek" value="Upravit Profil">
                    </div>
                </form>
            </div>
        </div>
    </div>

<?php
else :
    exit(header('Location: ' . BASE_URL));
endif;
?>
<?php

$uzivatele = Select::getInstance()->getAllUsers();
$role = Select::getInstance()->getRoleM();

$errors = array();
if (isset($_POST['submit'])) {

    foreach ($_POST['item'] as $item) {
        if (empty($item['id_role'])) {
            array_push($errors, 'Nebyla zadána role');
        }
        if (empty($item['id_uzivatele'])) {
            array_push($errors, 'Nehraj si s HTML :D');
        }

        if (empty($errors)) {
            $id = intval($item['id_uzivatele']);
            $id_role = intval($item['id_role']);
            $bo = Update::getInstance()->updateRole($id, $id_role);
            if (!$bo) {
                echo "Něco je špatně";
            }
        }
    }
    exit(header('Location: ' . BASE_URL . "?page=sprava_uzivatelu"));
}
?>
<div class="card">
    <div class="card-title">
        <h2>Správa uživatelů</h2>
    </div>
    <div class="card-body">
        <form method="post">
            <table>
                <tr>
                    <th>Login</th>
                    <th>Jméno</th>
                    <th>Příjmení</th>
                    <th>Role</th>
                </tr>
                <?php
                for ($i = 0; $i < count($uzivatele['LOGIN']); $i++) {
                ?>
                    <tr>
                        <input style="display: none;" name="item[<?php echo $i; ?>][id_uzivatele]" type="text" value="<?php echo $uzivatele['ID_UZIVATELE'][$i]; ?>">
                        <td><?php echo $uzivatele['LOGIN'][$i]; ?></td>
                        <th><?php echo $uzivatele['JMENO'][$i]; ?></th>
                        <td><?php echo $uzivatele['PRIJMENI'][$i]; ?></td>
                        <td><?php echo $uzivatele['ROLE'][$i]; ?></td>
                        <td>
                            <select name="item[<?php echo $i; ?>][id_role]">
                                <?php
                                for ($j = 0; $j < count($role['ID_ROLE']); $j++) {
                                    if ($role['ID_ROLE'][$j] > 4 || $role['ID_ROLE'][$j] < 3) {
                                ?>
                                        <option value="<?php echo $role['ID_ROLE'][$j]; ?>" <?php if ($uzivatele['ROLE'][$i] == $role['NAZEV'][$j]) { ?> selected <?php } ?>><?php echo $role['NAZEV'][$j]; ?></option>
                                <?php
                                    }
                                } ?>
                            </select>
                        </td>
                    </tr>
                <?php
                }
                ?>
            </table>
            <div class="form-submit">
                <input type="submit" value="Uložit změny" name="submit">
            </div>
        </form>
    </div>
</div>
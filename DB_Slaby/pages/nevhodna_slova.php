<?php if (!Authentication::getInstance()->isAdmin()) {
    exit(header('Location:' . BASE_URL . '?page=profile'));
}

$slova = Select::getInstance()->getSlova();

if (isset($_POST['pridat'])) {
    $slovo = strtolower($_POST['slovo']);
    if (!empty($slovo)) {
        Insert::getInstance()->InsertSlovo($slovo);
        exit(header('Location:' . BASE_URL . '?page=nevhodna_slova'));
    }else {
        echo '<span class="error-msg">Musíš vyplnit slovo</span>';
    }
}

if (isset($_GET['smaz']) && isset($_GET['id'])) {
    Delete::getInstance()->deleteSlovo($_GET['id']);
    exit(header('Location:' . BASE_URL . '?page=nevhodna_slova'));
}
?>



<div class="card">
    <div class="card-title">
        <h2>Nevhodná slova</h2>
    </div>
    <div class="card-body" style="width: 100%;">
        <?php
        if (!empty($slova)) {
        ?>
            <table class="predmety">
                <tr style="border-bottom: 1px black;">
                    <?php
                    echo "<th class='th_predmety' style='width: 20%;'>Slovo</th>";
                    echo "<th class='th_predmety'>Mazání</th>";
                    ?>
                </tr>
                <?php
                foreach ($slova as $item) {
                    $id = $item['ID_SLOVO'];
                    $s = $item['SLOVO'];

                    echo "<tr>";
                    echo "<td class='th_predmety'>" . $s . "</td>";
                    echo "<td class='th_predmety'><a onclick='return confirmDelete();' href='" . BASE_URL . "?page=nevhodna_slova&id=" . $id . "&smaz=1'>Smazat</a></td>";
                    echo "</tr>";
                }
                ?>
            </table>
        <?php
        } else {
            echo '<span class="error-msg">Nic zde není</span>';
        }
        ?>
        <?php if (Authentication::getInstance()->isAdmin()) : ?>
            <button onclick="ShowDialog()">Přidat slovo</button>
            <div class="card" style="display: none; margin-top: 15px;" id="dialog">
                <div class="card-title">
                    <h2>Přidání slova</h2>
                </div>
                <div class="card-body">
                    <form method="post">
                        <div class="form-group">
                            <label>Slovo:</label>
                            <input type="text" name="slovo" placeholder="Slovo">
                        </div>
                        <div class="form-submit">
                            <input type="submit" name="pridat" value="Přidat předmět">
                        </div>
                    </form>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>

<script>
    function confirmDelete() {
        return confirm("Opravdu chcete slovo smazat?\n");
    }
</script>
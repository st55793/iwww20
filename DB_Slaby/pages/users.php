<?php
$users = Select::getInstance()->getAllUsers();
$kontakty = Select::getInstance()->getKontakty();

if (isset($_GET['id_kontakt']) && isset($_GET['smaz'])) { // mazání
    $id_kontaktu = $_GET['id_kontakt'];
    Delete::getInstance()->deleteKontakt($id_kontaktu);
    exit(header('Location: ' . BASE_URL . '?page=users'));
}else if (isset($_GET['id_user'])) { // přidávání
    $id_kontaktu = $_GET['id_user'];
    Insert::getInstance()->InsertKontakt($id_kontaktu);
    exit(header('Location: ' . BASE_URL . '?page=users'));
}
?>

<div class="card">
    <div class="card-title">
        <h2>Přidávání / odebírání kontaktů</h2>
    </div>
    <div class="card-body" style="width: 100%;">
        <form method="post">
            <table>
                <tr>
                    <th>Login</th>
                    <th>Jméno</th>
                    <th>Příjmení</th>
                    <th>Role</th>
                    <th>Přidat kontakt</th>
                    <th>Odebrat kontakt</th>
                </tr>
                <?php
                for ($i = 0; $i < count($users['LOGIN']); $i++) {
                    if ($users['ID_UZIVATELE'][$i] != Authentication::getInstance()->getId()) {
                ?>
                        <tr>
                            <input style="display: none;" name="item[<?php echo $i; ?>][ID_UZIVATELE]" type="text" value="<?php echo $users['ID_UZIVATELE'][$i];?>">
                            <td><?php echo $users['LOGIN'][$i]; ?></td>
                            <th><?php echo $users['JMENO'][$i]; ?></th>
                            <td><?php echo $users['PRIJMENI'][$i]; ?></td>
                            <td><?php echo $users['ROLE'][$i]; ?></td>
                            <?php
                            $ok = false;
                            $z = 0;
                            foreach ($kontakty as $item) {
                                if($users['ID_UZIVATELE'][$i] == $item['ID_1'] || $users['ID_UZIVATELE'][$i] == $item['ID_2']){
                                    ?>
                                    <td>Už je v přátelích</td>
                                    <td><a onclick='return confirmDelete();' href="?page=users&smaz=1&id_kontakt=<?php echo $kontakty[$z]['ID_KONTAKT']; ?>">Odebrat kontakt</a></td>
                                    <?php
                                    $ok = true;
                                    break;
                                }
                                $z++;
                            }
                            if($ok == false){
                                ?>
                                <td><a href="?page=users&id_user=<?php echo $users['ID_UZIVATELE'][$i]; ?>">Přidat kontakt</a></td>
                                <td>nejste kamarádi</td></td>
                                <?php
                            }
                            ?>
                            
                        </tr>
                    <?php
                    }
                }
                ?>
            </table>
        </form>
    </div>
</div>
<script>
    function confirmDelete() {
        return confirm("Opravdu chcete kontakt smazat?\n");
    }
</script>
<?php if (Authentication::getInstance()->isMentor() || Authentication::getInstance()->isAdmin()) :
    $predmet = Select::getInstance()->getPredmet($_GET['id']);
    $id = $predmet[0]['ID_PREDMET'];
    $nazev = $predmet[0]['NAZEV'];
    $popis = $predmet[0]['POPIS'];
    $garant = Select::getInstance()->getGarantPrednasejici($id, 3);
    $prednasejici = Select::getInstance()->getGarantPrednasejici($id, 4);
    $ucitele = Select::getInstance()->getUcitele();
    $kontrola = 0;
    $success = "";

    if (isset($_POST['Upravit'])) {
        $naz =  trim($_POST['nazev']);
        $pop = trim($_POST['popis']);
        $gar = $_POST['garant'];
        $cvic = $_POST['cvicici'];

        if (empty($naz) || empty($pop)) {
            echo '<span class="error-msg">Musíš vyplnit nazev nebo popis bez toho to nepůjde</span>';
        } else {
            Delete::getInstance()->deleteOpravneni($id);
            if (Update::getInstance()->updatePredmet($id, $naz, $pop)) {
                foreach ($cvic as $item) {
                    if ($item != 0) {
                        if (Insert::getInstance()->InsertOpravnění($item, 4, $id)) {
                            $success = 'Cvičící upraveny';
                        }
                    }
                }
                if ($gar != 0) {
                    if (Insert::getInstance()->InsertOpravnění($gar, 3, $id)) {
                        $success = 'Garant upraven';
                    }
                }
                exit(header('Location:' . BASE_URL . '?page=predmet&id=' . $id));
            } else {
                echo "Něco se pokazilo :(";
            }
        }
    }
    if (isset($_POST['Cancel'])) {
        exit(header('Location:' . BASE_URL . '?page=predmety'));
    }
?>
    <div class="card">
        <div class="card-title">
            <h2>Úprava Předmětu</h2>
        </div>
        <div class="card-body">
            <form method="post">
                <div class="form-group">
                    <label>Název Předmětu:</label>
                    <input type="text" name="nazev" value="<?php echo $nazev; ?>">
                </div>
                <div class="form-group">
                    <label>Popis předmětu:</label>
                    <textarea style="margin-left: 75;" name="popis" cols="35" rows="5"><?php echo $popis; ?></textarea>
                </div>
                <hr style="margin-bottom: 10px; margin-top: 10px;">
                <div class="form-group">
                    <label style="padding-top: 7px;">Garant:</label>
                    <select name="garant">
                        <?php
                        if (empty($garant)) {
                        ?>
                            <option value="0" selected>Nikdo</option>
                        <?php
                        } else {
                        ?>
                            <option value="0">Nikdo</option>
                        <?php
                        }
                        foreach ($ucitele as $i) {
                        ?>
                            <option value="<?php echo $i['ID_UZIVATELE']; ?>" <?php if (!empty($garant)) {
                                                                                    if ($i['ID_UZIVATELE'] == $garant[0]['ID_UZIVATELE']) { ?> selected <?php }
                                                                                                                                                } ?>><?php echo $i['PRIJMENI']; ?></option>
                        <?php } ?>
                    </select>
                </div>
                <hr style="margin-bottom: 10px; margin-top: 10px;">
                <?php
                for ($g = 0; $g < 4; $g++) {
                ?>
                    <div class="form-group">
                        <label>Cvičící <?php echo $g+1;?>:</label>
                        <select name="cvicici[]">
                            <?php
                            $nacteno = false;
                            if ($kontrola == count($prednasejici)) {
                            ?>
                                <option value="0" selected>Nikdo</option>
                            <?php
                            } else {
                            ?>
                                <option value="0">Nikdo</option>
                            <?php
                            }

                            foreach ($ucitele as $i) {
                            ?>
                                <option value="<?php echo $i['ID_UZIVATELE']; ?>" <?php if ($kontrola < count($prednasejici)) {
                                                                                        if ($i['ID_UZIVATELE'] == $prednasejici[$kontrola]['ID_UZIVATELE'] && $nacteno == false) { ?> selected <?php $kontrola++;
                                                                                                                                                                                            $nacteno = true;
                                                                                                                                                                                        }
                                                                                                                                                                                    } ?>><?php echo $i['PRIJMENI']; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                <?php
                }
                ?>
                <div class="form-submit" style="float: left;">
                    <input type="submit" value="Upravit vše" name="Upravit">
                </div>
                <div class="form-submit" style="float: left; margin-left: 15px;">
                    <input style="background-color: #ff8000" type="submit" value="Zpět na výpis předmětů" name="Cancel">
                </div>
            </form>
        </div>
    </div>
<?php endif;
if (!empty($success)) {
    echo '<span class="error-msg">' . $success . '</span>';
}
?>
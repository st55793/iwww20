<?php
$warning = "";
$success = "";
if (isset($_POST['submit'])) {
    $hash_password = password_hash($_POST['new_pass'], PASSWORD_DEFAULT);
    $result = Authentication::getInstance()->GetPassword();
    if (trim($_POST['old_pass']) == "") {
        $warning = "Nebylo zadáno staré heslo";
    } else if (!password_verify($_POST['old_pass'], $result['HESLO'])) {
        $warning = "Zadáno špatné heslo";
    } else if (trim($_POST['new_pass']) == "") {
        $warning = "Nebylo zadáno nové heslo";
    } else if (trim($_POST['new_again']) == "") {
        $warning = "Nebylo zadáno kontrolní heslo";
    } else if ($_POST['new_pass'] != $_POST['new_again']) {
        $warning = "Zadaná hesla se neshodují";
    } else {
        if(Authentication::getInstance()->changePassword($hash_password)){
            $success = "Heslo bylo změněno";
        }else{
            $warning = "Někde vznikla chyba";
        }   
    }
}
if (empty($success)) {
    echo "<p><a href=". BASE_URL ."?page=profile class='back'>Zpět na profil</a></p>";
    if (!empty($warning)) {
        echo "<p class='message warning'>{$warning}</p>";
        $warning = "";
    }
    ?>
    <form method="post">
        <p><label>Staré heslo:</label><br><input type="password" name="old_pass" value="" /></p>
        <p><label>Nové heslo:</label><br><input type="password" name="new_pass" value="" /></p>
        <p><label>Nové heslo znovu:</label><br><input type="password" name="new_again" value="" /></p>
        <p><input type="submit" name="submit" value="Uložit" /></p>
    </form>
<?php
} else {
    echo "<p class='message success'>{$success}</p><a class='link' href='" . BASE_URL . "?page=logout'>Přihlásit se</a>";
}
?>
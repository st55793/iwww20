<?php if (Authentication::getInstance()->hasIdentity() == false) {
    exit(header('Location: ' . BASE_URL . "?page=login"));
}
if (!isset($_GET['id'])) {
    exit(header('Location: ' . BASE_URL . "?page=contacts"));
}

$kontakt = Select::getInstance()->getKontakt($_GET['id']);

if($kontakt['ID_1'] == Authentication::getInstance()->getId()){
    $id_prijemce = $kontakt['ID_2'];
}else{
    $id_prijemce = $kontakt['ID_1'];
}

//přidání zprávy
if (isset($_POST['odeslat_zpravu'])) {
    $text = $_POST['text'];
    if (!empty(trim($text))) {
        Insert::getInstance()->InsertZprava($text, $id_prijemce, $_GET['id']);
        
    } else {
        echo '<span class="error-msg">Zprávu nesmíš odeslat prázdnou</span>';
    }
}

//mazání
if(isset($_GET['smaz']) && isset($_GET['id_zpravy'])){
    Delete::getInstance()->deleteZprava($_GET['id_zpravy']);
    exit(header('Location: ' . BASE_URL . "?page=chat&id=".$_GET['id']));
}

$prijemce = Select::getInstance()->getUzivatel($id_prijemce);
$zpravy = Select::getInstance()->getZpravy($_GET['id']);
?>
<div class="card">
    <div class="card-title">
        <h2>Simply chat s <?php echo $prijemce['JMENO'] . ' ' . $prijemce['PRIJMENI'] ?></h2>
    </div>
    <div class="card-body" style="width: 100%;">
        <div class="card-body">
        <a href='?page=contacts'>Zpět na výpis kontaktů</a>
            <?php
            if (!empty($zpravy)) {
                foreach ($zpravy as $item) {
                    if ($item['ID_ODESILATELE'] == Authentication::getInstance()->getId()) {
                        echo '<div class="chat odesilatel">';
                    }else{
                        echo '<div class="chat prijemce">';
                    }
                    echo "<h4 style='margin-bottom: 3px;margin-top: 8px'>Uživatel/ka " . $item['JMENO_ODESILATELE'] . " napsal/a dne: " . $item['DATUM'] . " </h4>";
                    echo "<p>" . $item['TEXT'] . "</p>";
                    if ($item['ID_ODESILATELE'] == Authentication::getInstance()->getId()) {
                        echo "<a href='" . BASE_URL . "?page=uprav_zpravu&id=" . $item['ID_ZPRAVA'] . "'>Upravit</a>";
                        echo "<a onclick='return confirmDelete();' style='margin-left: 10px;' href='" . BASE_URL . "?page=chat&smaz=1&id=" . $_GET['id'] . "&id_zpravy=" . $item['ID_ZPRAVA'] . "'>Smazat</a>";
                    }
                    echo '</div>';
                }
            } else {
                echo '<span class="error-msg">Nic zde není</span>';
            }
            ?>
            <div class="chat"></div>
        </div>
        <button onclick="ShowDialog()">Přidat zprávu</button>
        <div class="card" style="display: none; margin-top: 15px;" id="dialog">
            <div class="card-title">
                <h4>Přidání zprávy</h4>
            </div>
            <div class="card-body">
                <form method="post">
                    <div class="form-group">
                        <label>Zpráva:</label>
                        <textarea style="margin-left: 75; min-width: 75%;" name="text" cols="30" rows="5" placeholder="Zde napiš zprávu"></textarea>
                    </div>
                    <div class="form-submit">
                        <input type="submit" name="odeslat_zpravu" value="Odeslat zprávu">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    function confirmDelete() {
        return confirm("Opravdu chcete zprávu smazat?\n");
    }
</script>
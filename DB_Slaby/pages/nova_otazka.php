<?php
if ((Authentication::getInstance()->isAdmin() || Authentication::getInstance()->isMentor()) && !empty($_GET)) :
    $typy = Select::getInstance()->getTypyOtazka();
    $id_kviz = $_GET['id_kvizu'];
    $message = 0;

    if (isset($_POST['pridat_otazku'])) {
        $message = 0;
        $otazka = $_POST['otazka'];
        $body = $_POST['body'];
        $odpoved = $_POST['odpoved'];
        $id_typ = $_POST['typ'];

        if (!empty($otazka) && $body != 0 && !empty(trim($odpoved[0]['odpoved']))) {

            Insert::getInstance()->InsertOtazka($otazka, $body, $id_kviz, $id_typ);
            $id_otazky = Select::getInstance()->getMaxIdOtazky();
            $isOk = true;
            $priznak = 0;
            $pocet_spravnych = 0;

            if ($id_typ == 1) { // jedna správná
                foreach ($odpoved as $item) {
                    $priznak = 0;
                    if (!empty($item['odpoved'])) {
                        if (isset($item['priznak']) && $isOk) {
                            $isOk = false;
                            $priznak = 1;
                            $pocet_spravnych++;
                        }
                        Insert::getInstance()->InsertOdpoved((string)$item['odpoved'], $priznak, $id_otazky);
                    }
                }
            } else if ($id_typ == 2) { // text
                $item = $odpoved[0];
                if (!empty($item['odpoved'])) {
                    $priznak = 1;
                    $pocet_spravnych++;
                    Insert::getInstance()->InsertOdpoved((string)$item['odpoved'], $priznak, $id_otazky);
                }
            } else if ($id_typ == 3) { // více správných
                foreach ($odpoved as $item) {
                    $priznak = 0;
                    if (!empty($item['odpoved'])) {
                        if (isset($item['priznak'])) {
                            $priznak = 1;
                            $pocet_spravnych++;
                        }
                        Insert::getInstance()->InsertOdpoved((string)$item['odpoved'], $priznak, $id_otazky);
                    }
                }
            }
            if ($pocet_spravnych > 0) {
                Update::getInstance()->updatePocetSpravnych($pocet_spravnych, $id_otazky);
            }
            $message = 1;
        } else {
            echo '<span class="error-msg">Vyplň všechna pole - min: otázku, body a alespoň jednu odpověd (první řádek pro odpověď)</span>';
        }
    }
?>
    <a href="<?php echo BASE_URL . '?page=uprav_kviz&id=' . $id_kviz ?>">
        < Zpět na úpravu kvízu</a> <div class="card">
            <div class="card-title" style="margin-top: 15px;">
                <h2>Přidání Otázky</h2>
            </div>
            <div class="card-body" style="width: 90%; margin: auto; margin-top: 20px;">
                <form method="post">
                    <div class="form-group">
                        <label class="label_otazka">Otázka:</label>
                        <input type="text" style="width: 65%;" name="otazka" placeholder="Otázka">
                    </div>
                    <div class="form-group">
                        <label class="label_otazka">Počet bodů za správnou odpověd:</label>
                        <input type="number" name="body" min="1" max="15" value="1">
                    </div>
                    <div class="form-group">
                        <label class="label_otazka">Typ otázky:</label>
                        <select name="typ">
                            <?php
                            foreach ($typy as $item) {
                            ?>
                                <option value="<?php echo $item['ID_TYP']; ?>"><?php echo $item['NAZEV']; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <hr style="margin-bottom: 10px; margin-top: 10px;">
                    <table>
                        <tr style="border-bottom: 1px black;">
                            <th class="th_predmety" style="width: 300px;">Odpověd</th>
                            <th class="th_predmety" style="width: auto;">Příznak správnosti</th>
                        </tr>
                        <?php for ($i = 0; $i < 10; $i++) { ?>
                            <tr style="border-bottom: 1px black; margin-top: 10px;">
                                <td class=""><input type="text" style="width: 100%;" name="odpoved[<?php echo $i; ?>][odpoved]" placeholder="Odpověď"></td>
                                <td style="text-align: center;"><input type="checkbox" name="odpoved[<?php echo $i; ?>][priznak]" value="1" /></td>
                            </tr>
                        <?php } ?>
                    </table>
                    <div class="form-submit">
                        <input type="submit" name="pridat_otazku" value="Přidat Otázku">
                    </div>
                </form>
                <?php
                if ($message != 0) {
                    echo '<span class="error-msg">Uloženo můžeš přidat další otázku</span>';
                }
                ?>
                <p style="margin-top: 10px;"><b>Vysvětlivky</b></p>
                <p style="margin-top: 10px;"><b>Body:</b> v případě více správných odpovědí je za každou správnou odpověd dán určitý počet bodů uvedený v bodech. v případě že jde o jednu správnou je dán počet bodů uvedený v bodech</p>
                <p style="margin-top: 10px;"><b>jedna správna</b> je brána jako správná odpověd ta první nalezená ostatní jsou ignorovány</p>
                <p style="margin-top: 10px;"><b>Více správných</b> tak jsou všechny nalezené odpovedi se zaškrtnutým políček příznak správnosti správné</p>
                <p style="margin-top: 10px;"><b>text</b> tak je brána jako správná odpověd pouze ta na prvním řádku</p>
            </div>
            </div>

        <?php
    endif;
        ?>
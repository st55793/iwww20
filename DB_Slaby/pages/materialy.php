<?php
if (!Authentication::getInstance()->hasIdentity()){
    exit(header('Location:' . BASE_URL . '?page=login'));
}
    $nazev = '';
    $prijmeni = '';
    $predmet = '';

    $materialy = Select::getInstance()->getMaterialy('%', '%', '%');
    $predmety = Select::getInstance()->getPredmetyMaterialy();
    $errors = array();
    if (isset($_GET['id']) && isset($_GET['smaz'])) {
        Delete::getInstance()->deleteMaterial($_GET['id']);
        exit(header('Location: ' . BASE_URL . "?page=materialy"));
    }

    if (isset($_POST['pridat'])) {
        $nazev = $_POST['nazev'];
        $popis = $_POST['popis'];
        $id_predmet = $_POST['predmet'];

        $file_upload = $_FILES['file']['tmp_name'];

        if ($file_upload) {
            $info = pathinfo($_FILES['file']['name']);
            $extension = $info['extension'];
            $name = $_FILES['file']['name'];
            $file = file_get_contents($file_upload);
        }

        if (empty(trim($nazev))) {
            array_push($errors, "musíš vyplnit název");
        }
        if (empty(trim($popis))) {
            array_push($errors, "musíš vyplnit popis");
        }
        if (!$file_upload) {
            array_push($errors, "musíš přidat soubor");
        }

        if (empty($errors)) {
            Insert::getInstance()->InsertMaterial($nazev, $popis, $id_predmet);

            if ($file_upload) {
                $conn = Connection::getConnection();
                $ID = intval(Authentication::getInstance()->getId());

                $query = oci_parse($conn, "INSERT INTO material_soubor VALUES(MATERIAL_SOUBOR_SEQ.NEXTVAL, empty_blob(), '{$name}', '{$extension}', CURRENT_TIMESTAMP, MATERIAL_SEQ.CURRVAL) RETURNING soubor INTO :file_upload");
                $blob = oci_new_descriptor($conn, OCI_D_LOB);
                oci_bind_by_name($query, ":file_upload", $blob, -1, OCI_B_BLOB);
                oci_execute($query, OCI_DEFAULT);
                if (!$blob->save($file)) {
                    oci_rollback($conn);
                } else {
                    oci_commit($conn);
                }
                oci_free_statement($query);
                $blob->free();
            }
            exit(header('Location: ' . BASE_URL . "?page=materialy"));
        } else {
            echo '<div class="card">';
            foreach ($errors as $error) {
                echo '<span class="error-msg">' . $error . '</span>';
            }
            echo '</div>';
            $errors = array();
        }
    }
    if (isset($_POST['vyhledat'])) {
        $nazev = '%' . $_POST['nazev_materialu'] . '%';
        $predmet = '%' . $_POST['nazev_predmetu'] .'%';
        $prijmeni = '%' . $_POST['prijmeni'] . '%';


        $materialy = Select::getInstance()->getMaterialy($nazev, $predmet, $prijmeni);

        $nazev = $_POST['nazev_materialu'];
        $predmet = $_POST['nazev_predmetu'];
        $prijmeni = $_POST['prijmeni'];
    }

?>

<div class="card">
    <div class="card-title">
        <h2>Studijní materiály</h2>
    </div>
    <div class="card-body" style="width: 100%; padding-bottom: 10px; border-bottom: 1px solid;">
        <div class="card-title">
            <h4>Vyhledávání:</h4>
        </div>
        <form method="post" enctype="multipart/form-data">
            <div class="form-group" style="float: left; margin-right: 7px;">
                <label style="text-align: center; margin:auto">Název:</label>
                <input type="text" name="nazev_materialu" placeholder="Název materiálu" value="<?php echo $nazev;?>">
            </div>
            <div class="form-group" style="float: left; margin-right: 7px;">
                <label style="text-align: center; margin:auto">předmět:</label>
                <input type="text" name="nazev_predmetu" placeholder="Název předmětu" value="<?php echo $predmet;?>">
            </div>
            <div class="form-group" style="float: left; margin-right: 7px;">
                <label style="text-align: center; margin:auto">Příjmení:</label>
                <input type="text" name="prijmeni" placeholder="Příjmení učitele" value="<?php echo $prijmeni;?>">
            </div>
            <div class="form-submit">
                <input type="submit" name="vyhledat" value="Vyhledat materiál">
            </div>
        </form>
    </div>
    <div class="card-body" style="width: 100%;">
        <table class="predmety">
            <?php if (!empty($materialy)) { ?>
                <tr style="border-bottom: 1px black;">
                    <th class="th_predmety" style="width: 25%;">Název materiálu</th>
                    <th class="th_predmety">Předmět</th>
                    <th class="th_predmety">Datum vytvoření</th>
                    <th class="th_predmety" style="width: 10%;">Vytvořil/a</th>
                    <?php if (Authentication::getInstance()->isMentor() || Authentication::getInstance()->isAdmin()) : ?>
                        <th class="th_predmety" style='width: 6%;'>Úprava</th>
                        <th class="th_predmety" style='width: 6%;'>Mazání</th>
                    <?php endif; ?>
                </tr>
            <?php
                $kontrola = array();
                foreach ($materialy as $item) {
                    $id = $item['ID_MATERIAL'];
                    $nazev = $item['NAZEV'];
                    $popis = $item['POPIS'];
                    $nazev_predmetu = $item['NAZEV_PREDMETU'];
                    $datum = $item['DATUM_VYTVORENI'];
                    $vytvoril = $item['PRIJMENI'];
                    echo "<tr>";
                    echo "<td class='th_predmety'><a href='" . BASE_URL . "?page=zobraz_material&id=" . $id . "'>" . $nazev . "</a></td>";
                    echo "<td class='th_predmety'>" . $nazev_predmetu . "</td>";
                    echo "<td class='th_predmety'>" . $datum . "</td>";
                    echo "<td class='th_predmety'>" . $vytvoril . "</td>";

                    if (Select::getInstance()->getKontrola($item['ID_PREDMET']) || Authentication::getInstance()->isAdmin()) {
                        echo "<td class='th_predmety'><a href='" . BASE_URL . "?page=uprav_material&id=" . $id . "'>Upravit</a></td>";
                        echo "<td class='th_predmety'><a onclick='return confirmDelete();' href='" . BASE_URL . "?page=materialy&id=" . $id . "&smaz=1'>Smazat</a></td>";
                    }
                    echo "</tr>";
                }
            } else {
                echo '<span class="error-msg">Nic zde není</span>';
            }
            ?>
        </table>
        <?php if ((Authentication::getInstance()->isAdmin() || Authentication::getInstance()->isMentor()) && !empty($predmety)) : ?>
            <button onclick="ShowDialog()">Přidat materiál</button>
            <div class="card" style="display: none; margin-top: 15px;" id="dialog">
                <div class="card-title">
                    <h2>Přidání studijního materiálu</h2>
                </div>
                <div class="card-body">
                    <form method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <label>Název materiálu:</label>
                            <input type="text" name="nazev" placeholder="Název materiálu">
                        </div>
                        <div class="form-group">
                            <label>Popis materiálu:</label>
                            <textarea style="margin-left: 50px; min-width: 30%;" name="popis" cols="30" rows="5" placeholder="Zde napiš popis materiálu"></textarea>
                        </div>
                        <div class="form-group">
                            <label>Předmět:</label>
                            <select name="predmet">
                                <?php
                                foreach ($predmety as $item) {
                                    echo "<option value='" . $item['ID_PREDMET'] . "'>" . $item['NAZEV_PREDMETU'] . "</option>";
                                } ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Soubor:</label>
                            <input type="file" name="file" placeholder="Nahrát soubor">
                        </div>
                        <div class="form-submit">
                            <input type="submit" name="pridat" value="Přidat materiál">
                        </div>
                    </form>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>
<script>
    function confirmDelete() {
        return confirm("Opravdu chcete materiál smazat?\n");
    }
</script>
<?php
$material = Select::getInstance()->getMaterial($_GET['id']);
$kategorie = Select::getInstance()->getKategorie();
$matkat = Select::getInstance()->getMatKat($_GET['id']);
$kvizy = Select::getInstance()->getKvizy($_GET['id']);
$errors = array();

if ((Authentication::getInstance()->isAdmin() || Authentication::getInstance()->isMentor()) && !empty($material)) :
    $nazev = $material['NAZEV'];
    $popis = $material['POPIS'];
    $predmet = $material['NAZEV_PREDMETU'];
    $id_predmet = $material['ID_PREDMET'];
    $id_material = $material['ID_MATERIAL'];
    $predmety = Select::getInstance()->getPredmetyMaterialy();
    $kontrola = 0;

    if (isset($_POST['upravit'])) {

        $nazev_u = $_POST['nazev'];
        $popis_u = $_POST['popis'];
        $id_predmet_u = $_POST['predmet'];
        $kat = $_POST['kategorie'];

        if (empty(trim($nazev_u))) {
            array_push($errors, 'Nebylo zadán název materiálu');
        }
        if (empty(trim($popis_u))) {
            array_push($errors, 'Nebylo zadán popis materiálu');
        }
        if (empty($errors)) {

            $file_upload = $_FILES['file']['tmp_name'];

            if ($file_upload) {
                $info = pathinfo($_FILES['file']['name']);
                $extension = $info['extension'];
                $name = $_FILES['file']['name'];
                $file = file_get_contents($file_upload);
            }

            if ($file_upload) {
                $conn = Connection::getConnection();
                $ID = intval(Authentication::getInstance()->getId());

                $query = oci_parse($conn, "INSERT INTO material_soubor VALUES(MATERIAL_SOUBOR_SEQ.NEXTVAL, empty_blob(), '{$name}', '{$extension}', CURRENT_TIMESTAMP, {$id_material}) RETURNING soubor INTO :file_upload");
                $blob = oci_new_descriptor($conn, OCI_D_LOB);
                oci_bind_by_name($query, ":file_upload", $blob, -1, OCI_B_BLOB);
                oci_execute($query, OCI_DEFAULT);
                if (!$blob->save($file)) {
                    oci_rollback($conn);
                } else {
                    oci_commit($conn);
                }
                oci_free_statement($query);
                $blob->free();
            }

            Update::getInstance()->updateMaterial($nazev_u, $popis_u, $id_predmet_u, $id_material);

            Delete::getInstance()->deleteMatKat($id_material);

            foreach ($kat as $item) {
                if ($item != 0) {
                    Insert::getInstance()->InsertMatKat($id_material, $item);
                }
            }

            exit(header('Location: ' . BASE_URL . "?page=uprav_material&id=" . $id_material));
        }
    }
    if (isset($_POST['pridat_kviz'])) {
        $nazev_kvizu = $_POST['nazev_kvizu'];
        $popis_kvizu = $_POST['popis_kvizu'];
        $pokus = $_POST['pokus'];

        if (!empty($nazev_kvizu) && !empty($popis_kvizu)) {
            Insert::getInstance()->InsertKviz($nazev_kvizu, $popis_kvizu, $id_material, $pokus);
            exit(header('Location: ' . BASE_URL . "?page=uprav_material&id=" . $id_material));
        } else {
            array_push($errors, 'Musíš vyplnit údaje o kvízu');
        }
    }

    if (isset($_GET['id_kviz']) && isset($_GET['smaz'])) {
        Delete::getInstance()->deleteKviz($_GET['id_kviz']);
        exit(header('Location: ' . BASE_URL . "?page=uprav_material&id=" . $id_material));
    }

    if (!empty($errors)) {
        echo '<div class="card">';
        foreach ($errors as $error) {
            echo '<span class="error-msg">' . $error . '</span>';
        }
        echo '</div>';
        $errors = array();
    }
?>
    <a style="margin-top: 5px;" href="<?php echo BASE_URL . '?page=materialy' ?>">
        < Zpět k materiálům</a> <div class="card">
            <div class="card-title">
                <h2>Úprava studijního materiálu</h2>
            </div>
            <div style="width: 80%;" class="card-body">
                <form method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label>Název materiálu:</label>
                        <input style="width: 80%;" type="text" name="nazev" value="<?php echo $nazev; ?>">
                    </div>
                    <div class="form-group">
                        <label>Popis materiálu:</label>
                        <input style="width: 80%;" type="text" name="popis" value="<?php echo $popis; ?>">
                    </div>
                    <div class="form-group">
                        <label>Předmět:</label>
                        <select style="width: 80%;" name="predmet">
                            <?php
                            $kon = array();
                            foreach ($predmety as $item) {
                                $isOk = true;
                                foreach ($kon as $i) {
                                    if ($i == $item['ID_PREDMET']) {
                                        $isOk = false;
                                    }
                                }
                                if ($isOk) {
                            ?>
                                    <option value=<?php echo $item['ID_PREDMET'];
                                                    if ($item['ID_PREDMET'] == $id_predmet) { ?> selected <?php } ?>> <?php echo $item['NAZEV_PREDMETU']; ?></option>
                            <?php
                                }
                                array_push($kon, $item['ID_PREDMET']);
                            } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Soubor:</label>
                        <input type="file" name="file" placeholder="Nahrát soubor">
                    </div>
                    <hr style="margin-bottom: 10px; margin-top: 10px;">
                    <?php
                    for ($g = 1; $g < 5; $g++) {
                    ?>
                        <div class="form-group">
                            <label>Kategorie <?php echo $g . ":"; ?></label>
                            <select style="width: 80%;" name="kategorie[]">
                                <?php
                                $nacteno = false;
                                if ($kontrola == count($matkat)) {
                                ?>
                                    <option value="0" selected>Nic</option>
                                <?php
                                } else {
                                ?>
                                    <option value="0">Nic</option>
                                <?php
                                }

                                foreach ($kategorie as $i) {
                                ?>
                                    <option value="<?php echo $i['ID_KATEGORIE']; ?>" <?php if ($kontrola < count($matkat)) {
                                                                                            if ($i['ID_KATEGORIE'] == $matkat[$kontrola]['ID_KATEGORIE'] && $nacteno == false) {
                                                                                        ?> selected <?php
                                                                                                $kontrola++;
                                                                                                $nacteno = true;
                                                                                            }
                                                                                        } ?>><?php echo $i['NAZEV']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    <?php } ?>
                    <div class="form-submit">
                        <input type="submit" name="upravit" value="Upravit materiál">
                    </div>
                </form>
            </div>
            <hr style="margin-bottom: 10px; margin-top: 10px;">
            <div class="card-title">
                <h2>Kvízy k materiálu</h2>
            </div>
            <div style="width: 80%;" class="card-body">
                <table class="predmety">
                    <?php if (!empty($kvizy)) { ?>
                        <tr style="border-bottom: 1px black;">
                            <th class="th_predmety">Název</th>
                            <?php if (Authentication::getInstance()->isMentor() || Authentication::getInstance()->isAdmin()) : ?>
                                <th class="th_predmety">Úprava</th>
                                <th class="th_predmety">Mazání</th>
                            <?php endif; ?>
                        </tr>
                    <?php
                        $kontrola = array();
                        foreach ($kvizy as $item) {
                            $isOk = true;
                            $id = $item['ID_KVIZ'];
                            $nazev = $item['NAZEV'];
                            $popis = $item['POPIS'];
                            if ($isOk) {
                                echo "<tr>";
                                echo "<td class='th_predmety' style='width: 60%;'>" . $nazev . "</td>";
                                echo "<td class='th_predmety'><a href='" . BASE_URL . "?page=uprav_kviz&id=" . $id . "'>Upravit</a></td>";
                                echo "<td class='th_predmety'><a href='" . BASE_URL . "?page=uprav_material&id=" . $id_material . "&smaz=1&id_kviz=" . $id . "'>Smazat</a></td>";
                                echo "</tr>";
                            }
                        }
                    } else {
                        echo '<span class="error-msg">Nic zde není</span>';
                    }
                    ?>
                </table>
            </div>
            <button onclick="ShowDialog()">Přidat Kvíz</button>
            <div class="card" style="display: none; margin-top: 15px;" id="dialog">
                <div class="card-title">
                    <h2>Přidání kvízu</h2>
                </div>
                <div class="card-body">
                    <form method="post">
                        <div class="form-group">
                            <label>Název Kvízu:</label>
                            <input type="text" style="width: 80%;" name="nazev_kvizu" placeholder="Název Kvízu">
                        </div>
                        <div class="form-group">
                            <label>Popis Kvízu:</label>
                            <textarea style="margin-left: 17px;" placeholder="Popis kvízu" style="resize: none;" rows="5" name="popis_kvizu"></textarea>
                        </div>
                        <div class="form-group">
                            <label class="label_otazka">Počet pokusů:</label>
                            <input type="number" name="pokus" min="1" max="15" value="1">
                        </div>
                        <div class="form-submit">
                            <input type="submit" name="pridat_kviz" value="Přidat Kvíz">
                        </div>
                    </form>
                </div>
            </div>
            </div>
        <?php endif; ?>
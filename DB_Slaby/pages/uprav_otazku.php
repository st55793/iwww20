<?php
if ((Authentication::getInstance()->isAdmin() || Authentication::getInstance()->isMentor()) && !empty($_GET)) :
    $typy = Select::getInstance()->getTypyOtazka();
    $otazka = Select::getInstance()->getOtazka($_GET['id']);
    $odpovedi = Select::getInstance()->getOdpovedi($otazka['ID_OTAZKA']);
    $id_otazky = $otazka['ID_OTAZKA'];
    $message = 0;

    if (isset($_POST['upravit_otazku'])) {
        $message = 0;
        $otazka_u = $_POST['otazka'];
        $body_u = $_POST['body'];
        $odpoved = $_POST['odpoved'];
        $id_typ = $_POST['typ'];

        if (!empty($otazka_u) && $body_u != 0 && !empty(trim($odpoved[0]['odpoved']))) {
            $pocet_spravnych = 0;
            $isOk = true;
            $priznak = 0;
            Update::getInstance()->updateOtazka($otazka['ID_OTAZKA'], $otazka_u, $body_u, $id_typ);

            Delete::getInstance()->deleteOdpoved($otazka['ID_OTAZKA']);

            if ($id_typ == 1) { // jedna správná
                foreach ($odpoved as $item) {
                    $priznak = 0;
                    if (!empty($item['odpoved'])) {
                        if (isset($item['priznak']) && $isOk) {
                            $isOk = false;
                            $priznak = 1;
                            $pocet_spravnych++;
                        }
                        Insert::getInstance()->InsertOdpoved((string)$item['odpoved'], $priznak, $id_otazky);
                    }
                }
            } else if ($id_typ == 2) { // text
                $item = $odpoved[0];
                if (!empty($item['odpoved'])) {
                    $priznak = 1;
                    $pocet_spravnych++;
                    Insert::getInstance()->InsertOdpoved((string)$item['odpoved'], $priznak, $id_otazky);
                }
            } else if ($id_typ == 3) { // více správných
                foreach ($odpoved as $item) {
                    $priznak = 0;
                    if (!empty($item['odpoved'])) {
                        if (isset($item['priznak'])) {
                            $priznak = 1;
                            $pocet_spravnych++;
                        }
                        Insert::getInstance()->InsertOdpoved((string)$item['odpoved'], $priznak, $id_otazky);
                    }
                }
            }

            Update::getInstance()->updatePocetSpravnych($pocet_spravnych, $id_otazky);

            $otazka = Select::getInstance()->getOtazka($_GET['id']);
            $odpovedi = Select::getInstance()->getOdpovedi($otazka['ID_OTAZKA']);

            $message = 1;
        } else {
            echo '<span class="error-msg">Někde se něco pokazilo :(</span>';
        }
    }
?>
    <a style="margin-top: 5px;" href="<?php echo BASE_URL . '?page=uprav_kviz&id=' . $otazka['ID_KVIZ'] ?>">
        < Zpět na úpravu kvízu</a> <div class="card">
            <div class="card-title">
                <h2 style="margin-top: 15px;">Úprava otázky</h2>
            </div>
            <div class="card-body" style="width: 90%; margin: auto; margin-top: 20px;">
                <form method="post">
                    <div class="form-group">
                        <label class="label_otazka">Otázka:</label>
                        <input type="text" style="width: 65%;" name="otazka" placeholder="Otázka" value="<?php echo $otazka['OTAZKA']; ?>">
                    </div>
                    <div class="form-group">
                        <label class="label_otazka">Počet bodů za správnou odpověd:</label>
                        <input type="number" name="body" min="1" max="10" value="<?php echo $otazka['BODY']; ?>">
                    </div>
                    <div class="form-group">
                        <label class="label_otazka">Typ otázky:</label>
                        <select name="typ">
                            <?php
                            foreach ($typy as $item) {
                            ?>
                                <option value="<?php echo $item['ID_TYP']; ?>" <?php if ($item['ID_TYP'] == $otazka['ID_TYP']) { ?>selected<?php } ?>><?php echo $item['NAZEV']; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <hr style="margin-bottom: 10px; margin-top: 10px;">
                    <table>
                        <tr style="border-bottom: 1px black;">
                            <th class="th_predmety" style="width: 300px;">Odpověd</th>
                            <th class="th_predmety" style="width: auto;">Příznak správnosti</th>
                        </tr>
                        <?php for ($i = 0; $i < 10; $i++) { ?>
                            <tr style="border-bottom: 1px black; margin-top: 10px;">
                                <td class=""><input type="text" style="width: 100%;" name="odpoved[<?php echo $i; ?>][odpoved]" placeholder="Odpověď" value="<?php if (isset($odpovedi[$i]['ODPOVED'])) {
                                                                                                                                                                    echo $odpovedi[$i]['ODPOVED'];
                                                                                                                                                                } ?>"></td>
                                <td style="text-align: center;"><input type="checkbox" name="odpoved[<?php echo $i; ?>][priznak]" value="1" <?php if (isset($odpovedi[$i]['PRIZNAK']) && $odpovedi[$i]['PRIZNAK'] == 1) { ?> checked="checked" <?php } ?> /></td>
                            </tr>
                        <?php } ?>
                    </table>
                    <div class="form-submit">
                        <input type="submit" name="upravit_otazku" value="Upravit otázku">
                    </div>
                </form>
                <?php
                if ($message != 0) {
                    echo '<span class="error-msg">Uloženo můžeš se vrátit zpět</span>';
                }
                ?>
                <p style="margin-top: 10px;"><b>Vysvětlivky</b></p>
                <p style="margin-top: 10px;"><b>Body:</b> v případě více správných odpovědí je za každou správnou odpověd dán určitý počet bodů uvedený v bodech. v případě že jde o jednu správnou je dán počet bodů uvedený v bodech</p>
                <p style="margin-top: 10px;"><b>jedna správna</b> je brána jako správná odpověd ta první nalezená ostatní jsou ignorovány</p>
                <p style="margin-top: 10px;"><b>Více správných</b> tak jsou všechny nalezené odpovedi se zaškrtnutým políček příznak správnosti správné</p>
                <p style="margin-top: 10px;"><b>text</b> tak je brána jako správná odpověd pouze ta na prvním řádku</p>
            </div>
            </div>

        <?php
    endif;
        ?>
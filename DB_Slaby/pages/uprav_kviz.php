<?php
if (!isset($_GET['id'])) {
    exit(header('Location: ' . BASE_URL . "?page=materialy"));
}
if ((Authentication::getInstance()->isAdmin() || Authentication::getInstance()->isMentor()) && !empty($_GET)) :
    $id_kviz = $_GET['id'];

    $kviz = Select::getInstance()->getKviz($id_kviz);
    $nazev = $kviz['NAZEV'];
    $popis = $kviz['POPIS'];
    $pokus = $kviz['POCET_POKUSU'];

    $otazky = Select::getInstance()->getOtazky($id_kviz);
    $kontrola = 0;

    if (isset($_POST['upravit'])) {
        if (!empty($_POST['nazev']) && !empty($_POST['popis'])) {
            Update::getInstance()->updateKviz($id_kviz, $_POST['nazev'], $_POST['popis'], $_POST['pokus']);
            exit(header('Location: ' . BASE_URL . "?page=uprav_kviz&id=" . $id_kviz));
        }
    }
    if (isset($_POST['nova_otazka'])) {
        exit(header('Location: ' . BASE_URL . "?page=nova_otazka&id_kvizu=" . $id_kviz));
    }

    if (isset($_GET['id_otazky']) && isset($_GET['smaz'])) {
        Delete::getInstance()->deleteOtazka($_GET['id_otazky']);
        exit(header('Location: ' . BASE_URL . "?page=uprav_kviz&id=" . $id_kviz));
    }

?>
    <a style="margin-top: 5px;" href="<?php echo BASE_URL . '?page=uprav_material&id=' . $kviz['ID_MATERIAL'] ?>">
        < Zpět na úpravu materiálu</a> <div class="card">
            <div class="card-title">
                <h2>Úprava Kvízu</h2>
            </div>
            <div style="width: 80%;" class="card-body">
                <form method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label>Název kvízu:</label>
                        <input style="width: 80%;" type="text" name="nazev" value="<?php echo $nazev; ?>">
                    </div>
                    <div class="form-group">
                        <label>Popis kvízu:</label>
                        <input style="width: 80%;" type="text" name="popis" value="<?php echo $popis; ?>">
                    </div>
                    <div class="form-group">
                        <label class="label_otazka">Počet pokusů:</label>
                        <input type="number" name="pokus" min="1" max="15" value="<?php echo $pokus; ?>">
                    </div>
                    <hr style="margin-bottom: 10px; margin-top: 10px;">
                    <div class="form-submit">
                        <input type="submit" name="upravit" value="Upravit Kvíz">
                    </div>
                </form>
            </div>
            <hr style="margin-bottom: 10px; margin-top: 10px;">
            <div class="card-title">
                <h2>Otázky ke kvízu</h2>
            </div>
            <div style="width: 80%;" class="card-body">
                <table class="predmety">
                    <?php if (!empty($otazky)) { ?>
                        <tr style="border-bottom: 1px black;">
                            <th class="th_predmety">Název Otázky</th>
                            <th class="th_predmety">Typ otázky</th>
                            <?php if (Authentication::getInstance()->isMentor() || Authentication::getInstance()->isAdmin()) : ?>
                                <th class="th_predmety">Úprava</th>
                                <th class="th_predmety">Mazání</th>
                            <?php endif; ?>
                        </tr>
                    <?php
                        $kontrola = array();
                        foreach ($otazky as $item) {
                            $isOk = true;
                            $id = $item['ID_OTAZKA'];
                            $otazka = $item['OTAZKA'];
                            $nazev_typu = $item['NAZEV'];
                            $body = $item['BODY'];
                            if ($isOk) {
                                echo "<tr>";
                                echo "<td class='th_predmety' style='width: 60%;'>" . $otazka . "</td>";
                                echo "<td class='th_predmety' style='width: 60%;'>" . $nazev_typu . "</td>";
                                echo "<td class='th_predmety'><a href='" . BASE_URL . "?page=uprav_otazku&id=" . $id . "'>Upravit</a></td>";
                                echo "<td class='th_predmety'><a href='" . BASE_URL . "?page=uprav_kviz&id_otazky=" . $id . "&id=" . $id_kviz . "&smaz=1'>Smazat</a></td>";
                                echo "</tr>";
                            }
                        }
                    } else {
                        echo '<span class="error-msg">Nic zde není</span>';
                    }
                    ?>
                </table>
                <form method="POST">
                    <input type="submit" name="nova_otazka" value="Přidat novou otázku" />
                </form>
            </div>
            </div>
        <?php endif; ?>
<?php
$komentar = Select::getInstance()->getKomentar($_GET['id']);
if((Authentication::getInstance()->isAdmin() || Authentication::getInstance()->isMentor()) || $komentar['ID_UZIVATELE'] == Authentication::getInstance()->getId()) :

    if(isset($_POST['upravit_komentar'])){
        $text = trim($_POST['text']);
        if (!empty($text)) {
            Update::getInstance()->updateKomentar($_GET['id'], $text);
            exit(header('Location: ' . BASE_URL . "?page=zobraz_material&id=" . $komentar['ID_MATERIAL']));
        }
    }
?>
<div class="card" style="margin-top: 15px;" id="dialog">
    <div class="card-title">
        <h3>Úprava Komentáře</h3>
    </div>
    <div class="card-body">
        <form method="post">
            <div class="form-group">
                <label>Text komentáře:</label>
                <textarea style="margin-left: 17px;" placeholder="Text komentáře" style="resize: none;" rows="5" name="text"><?php echo $komentar['TEXT']; ?></textarea>
            </div>
            <div class="form-submit">
                <input type="submit" name="upravit_komentar" value="Upravit komentář">
            </div>
        </form>
    </div>
</div>
<?php
else:
    exit(header('Location: ' . BASE_URL . "?page=zobraz_material&id=" . $komentar['ID_MATERIAL']));
endif;
?>

<?php
$vysledky = Select::getInstance()->getvysledky('%', '%', '%');
$predmety = Select::getInstance()->getPredmetyMaterialy();
$nazev_kvizu = '';
$prijmeni = '';
$predmet = '';

if (isset($_POST['vyhledat'])) {
    $nazev_kvizu = '%' . $_POST['nazev_kvizu'] . '%';
    $predmet = '%' . $_POST['predmet'] . '%';
    $prijmeni = '%' . $_POST['prijmeni'] . '%';
    if ($predmet == '%0%') {
        $predmet = '%';
    }
    $vysledky = Select::getInstance()->getvysledky($predmet, $nazev_kvizu, $prijmeni);

    $nazev_kvizu = $_POST['nazev_kvizu'];
    $predmet = $_POST['predmet'];
    $prijmeni = $_POST['prijmeni'];
}

if(isset($_GET['smaz']) && isset($_GET['id'])){
    Delete::getInstance()->deleteVysledek($_GET['id']);
    exit(header('Location:' . BASE_URL . '?page=vysledky'));
}
?>

<div class="card">
    <div class="card-title">
        <h2>Kvízové výsledky</h2>
    </div>
    <div class="card-body" style="width: 100%; padding-bottom: 10px; border-bottom: 1px solid;">
        <div class="card-title">
            <h4>Prohledávání výsledků:</h4>
        </div>
        <form method="post" enctype="multipart/form-data">
            <div class="form-group" style="float: left; margin-right: 7px;">
                <label style="text-align: center; margin:auto">Název kvízu:</label>
                <input type="text" name="nazev_kvizu" placeholder="Název kvízu" value="<?php echo $nazev_kvizu; ?>">
            </div>
            <div class="form-group" style="float: left; margin-right: 7px;">
                <label style="text-align: center; margin:auto">předmět:</label>
                <select name="predmet">
                    <option value="0">Nic</option>
                    <?php
                    foreach ($predmety as $item) {
                        if ($item['NAZEV_PREDMETU'] == $predmet) {
                            echo "<option value='" . $item['NAZEV_PREDMETU'] . "' selected>" . $item['NAZEV_PREDMETU'] . "</option>";
                        } else {
                            echo "<option value='" . $item['NAZEV_PREDMETU'] . "'>" . $item['NAZEV_PREDMETU'] . "</option>";
                        }
                    } ?>
                </select>
            </div>
            <div class="form-group" style="float: left; margin-right: 7px;">
                <label style="text-align: center; margin:auto">Příjmení:</label>
                <input type="text" name="prijmeni" placeholder="Příjmení žáka" value="<?php echo $prijmeni; ?>">
            </div>
            <div class="form-submit">
                <input type="submit" name="vyhledat" value="Vyhledat">
            </div>
        </form>
    </div>
</div>
<?php if (!empty($vysledky)) : ?>
    <div class="card">
        <div class="card-body" style="width: 100%;">
            <table class="predmety">
                <tr style="border-bottom: 1px black;">
                    <?php
                    echo "<th class='th_predmety' style='width: 28%;'>Nazev kvízu</th>";
                    echo "<th class='th_predmety' style='width: 22%;'>Název předmětu</th>";
                    echo "<th class='th_predmety' style='width: 10%'>Jméno</th>";
                    echo "<th class='th_predmety' style='width: 10%'>Příjmení</th>";
                    echo "<th class='th_predmety' style='width: 12%'>Počet bodů</th>";
                    echo "<th class='th_predmety' style='width: 10%'>S/Č/Š</th>";
                    if (Authentication::getInstance()->isMentor() || Authentication::getInstance()->isAdmin()) {
                        echo "<th class='th_predmety'>Mazání</th>";
                    }
                    ?>
                </tr>
                <?php
                foreach ($vysledky as $item) {
                    $id_vysledek = $item['ID_VYSLEDEK'];
                    $nazev_kvizu = $item['NAZEV_KVIZU'];
                    $nazev_predmetu = $item['NAZEV_PREDMETU'];
                    $jmeno = $item['JMENO'];
                    $prijmeni = $item['PRIJMENI'];
                    $body = $item['VYSLEDEK'] . ' ze ' . $item['POCET_BODU'] . ' Bodů';
                    $SCS = $item['SPRAVNE'] . '/' . $item['CASTECNE'] . '/' . $item['SPATNE'];

                    echo "<tr>";
                    echo "<td class='th_predmety'>" . $nazev_kvizu . "</td>";
                    echo "<td class='th_predmety'>" . $nazev_predmetu . "</td>";
                    echo "<td class='th_predmety'>" . $jmeno . "</td>";
                    echo "<td class='th_predmety'>" . $prijmeni . "</td>";
                    echo "<td class='th_predmety'>" . $body . "</td>";
                    echo "<td class='th_predmety'>" . $SCS . "</td>";
                    if (Authentication::getInstance()->isMentor() || Authentication::getInstance()->isAdmin()) {
                        echo "<td class='th_predmety'><a onclick='return confirmDelete();' href='" . BASE_URL . "?page=vysledky&id=" . $id_vysledek . "&smaz=1'>Smazat</a></td>";
                    }
                    echo "</tr>";
                }
                ?>
            </table>
        </div>
    </div>
<?php else : echo '<span class="error-msg">Nic nenalezeno</span>';
endif ?>
<script>
    function confirmDelete() {
        return confirm("Opravdu chcete výsledek smazat?\n");
    }
</script>
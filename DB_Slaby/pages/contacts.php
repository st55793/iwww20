<?php
$kontakty = Select::getInstance()->getKontakty();
if (isset($_POST['pridat'])) {
    exit(header('Location: ' . BASE_URL . "?page=users"));
}
?>
<p><a href="?page=profile" class="back">Zpět na profil</a></p>
<div class="table">
    <?php
    if (!empty($kontakty)) {
    ?>
        <table>
            <tr>
                <th>Jméno</th>
                <th>Příjmení</th>
                <th>Chat</th>
            </tr>
            <?php
            for ($i = 0; $i < count($kontakty); $i++) {
                if($kontakty[$i]["ID_1"] == Authentication::getInstance()->getId()){
                    echo "<tr><td>{$kontakty[$i]["JMENO_2"]}</td><td>{$kontakty[$i]["PRIJMENI_2"]}</td><td><a href='?page=chat&id={$kontakty[$i]["ID_KONTAKT"]}'>Chat</a></td></tr>";
                }else{
                    echo "<tr><td>{$kontakty[$i]["JMENO_1"]}</td><td>{$kontakty[$i]["PRIJMENI_1"]}</td><td><a href='?page=chat&id={$kontakty[$i]["ID_KONTAKT"]}'>Chat</a></td></tr>";
                }
            }
            ?>
        </table>
    <?php
    } else {
        echo '<span class="error-msg">Nemáš žádné kamarády</span>';
    }
    ?>
    <form method="post" enctype="multipart/form-data">
        <div class="form-group">
        <div class="form-submit">
            <input type="submit" name="pridat" value="Přidat / odebrat kamarády">
        </div>
    </form>
</div>
<script>
    function confirmDelete() {
        return confirm("Opravdu chcete zprávu smazat?\n");
    }
</script>
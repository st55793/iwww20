<?php
if (Authentication::getInstance()->hasIdentity()) :
    $material = Select::getInstance()->getMaterial($_GET['id']);
    $kvizy = Select::getInstance()->getKvizy($material['ID_MATERIAL']);
    $komentare = Select::getInstance()->getKomentare($material['ID_MATERIAL']);
    $slova = Select::getInstance()->getSlova();

    if (isset($_POST['pridat_komentar'])) {
        $text = $_POST['text'];
        if (!empty($text)) {
            $isOk = true;
            $explode = explode(" ", $text);
            foreach ($explode as $item) { // kontrola proti nevhodným slovům
                foreach ($slova as $s) {
                    if (strtolower($s['SLOVO']) == strtolower($item)) {
                        $isOk = false;
                        break;
                    }
                }
                if ($isOk == false) {
                    break;
                }
            }
            if($isOk){
                Insert::getInstance()->InsertKomentar($text, $material['ID_MATERIAL']);
                exit(header('Location: ' . BASE_URL . "?page=zobraz_material&id=" . $material['ID_MATERIAL']));
            }else {
                echo '<span class="error-msg">Nemluv sprostě</span>';
            }
        } else {
            echo '<span class="error-msg">Musíš vyplnit komentář</span>';
        }
    }

    if (isset($_GET['smaz']) && isset($_GET['id_komentar'])) {
        Delete::getInstance()->deleteKomentar($_GET['id_komentar']);
        exit(header('Location: ' . BASE_URL . "?page=zobraz_material&id=" . $material['ID_MATERIAL']));
    }
?>
    <a style="margin-top: 5px;" href="<?php echo BASE_URL . '?page=materialy' ?>">
        < Zpět k materiálům</a> <?php
                                echo "<h3 style='margin-top: 10px;'>Materiál - " . $material['NAZEV'] . "</h3>";
                                echo "<h4 style='margin-top: 10px;'>Popis</h4>";
                                echo "<p style='margin-top: 5px;'>" . $material['POPIS'] . "</p>";
                                echo "<h4 style='margin-top: 10px; margin-bottom: 5px;'>Soubor</h4>";
                                echo "<a href='data:application/" . $material['TYP_SOUBORU'] . ";base64," . base64_encode($material['SOUBOR']) . "' download='" . $material['NAZEV_SOUBORU'] . "'>" . $material['NAZEV_SOUBORU'] . "</a></a><span>\t (Po kliknutí stáhnete)</span>";
                                echo "<h4 style='margin-top: 10px;'>Udaje o souboru</h4>";
                                echo "<p style='margin-top: 5px;'>název: " . $material['NAZEV_SOUBORU'] . ", typ: " . $material['TYP_SOUBORU'] . ", datum přidání: " . $material['DATUM_SOUBOR'] . "</p>";
                                echo "<h4 style='margin-top: 10px;'>Datum Vytvoření</h4>";
                                echo "<p style='margin-top: 5px;'>" . $material['DATUM_VYTVORENI'] . "</p>";
                                echo "<h4 style='margin-top: 10px;'>Datum poslední změny</h4>";
                                echo "<p style='margin-top: 5px;'>" . $material['DATUM_ZMENY'] . "</p>";
                                ?> <hr style="margin-bottom: 10px; margin-top: 10px;">
            <div class="card-title">
                <h2>Kvízy k materiálu</h2>
            </div>
            <div style="width: 80%;" class="card-body">
                <table class="predmety">
                    <?php if (!empty($kvizy)) { ?>
                        <tr style="border-bottom: 1px black;">
                            <th class="th_predmety">Název</th>
                            <?php if (Authentication::getInstance()->isMentor() || Authentication::getInstance()->isAdmin()) : ?>
                                <th class="th_predmety">Popis</th>
                            <?php endif; ?>
                        </tr>
                    <?php
                        $kontrola = array();
                        foreach ($kvizy as $item) {
                            $isOk = true;
                            $id = $item['ID_KVIZ'];
                            $nazev = $item['NAZEV'];
                            $popis = $item['POPIS'];
                            if ($isOk) {
                                echo "<tr>";
                                echo "<td class='th_predmety' style='width: 60%;'><a href='" . BASE_URL . "?page=vstup_kviz&id_kviz=" . $id . "'</a>" . $nazev . "</td>";
                                echo "<td class='th_predmety' style='width: 60%;'>" . $popis . "</td>";
                                echo "</tr>";
                            }
                        }
                    } else {
                        echo '<span class="error-msg">Nic zde není</span>';
                    }
                    ?>
                </table>
            </div>
            <div class="card-title">
                <h2>Komentáře k materiálu</h2>
            </div>
            <button onclick="ShowDialog()">Přidat Komentář</button>
            <div class="card" style="display: none; margin-top: 15px;" id="dialog">
                <div class="card-title">
                    <h3>Přidání Komentáře</h3>
                </div>
                <div class="card-body">
                    <form method="post">
                        <div class="form-group">
                            <label>Text komentáře:</label>
                            <textarea style="margin-left: 17px;" placeholder="Text komentáře" style="resize: none;" rows="5" name="text"></textarea>
                        </div>
                        <div class="form-submit">
                            <input type="submit" name="pridat_komentar" value="Přidat komentář">
                        </div>
                    </form>
                </div>
            </div>
            <div style="width: 80%;" class="card-body">
                <?php if (!empty($komentare)) {
                    foreach ($komentare as $item) {
                        if ($item['ID_UZIVATELE'] == Authentication::getInstance()->getId()) {
                            echo '<div class="chat odesilatel">';
                        } else {
                            echo '<div class="chat prijemce">';
                        }
                        echo "<h4 style='margin-bottom: 3px;margin-top: 8px'>Uživatel/ka " . $item['JMENO'] . " " . $item['PRIJMENI']  . " napsal/a: </h4>";
                        echo "<p>" . $item['TEXT'] . "</p>";
                        if ((Authentication::getInstance()->isAdmin() || Authentication::getInstance()->isMentor()) || $item['ID_UZIVATELE'] == Authentication::getInstance()->getId()) {
                            echo "<a href='" . BASE_URL . "?page=uprav_komentar&id=" . $item['ID_KOMENTAR'] . "'>Upravit</a>";
                            echo "<a style='margin-left: 10px;' href='" . BASE_URL . "?page=zobraz_material&smaz=1&id=" . $material['ID_MATERIAL'] . "&id_komentar=" . $item['ID_KOMENTAR'] . "'>Smazat</a>";
                        }
                        echo '</div>';
                    }
                } else {
                    echo '<span class="error-msg">Nic zde není</span>';
                }
                ?>
            </div>
        <?php
    endif;
        ?>
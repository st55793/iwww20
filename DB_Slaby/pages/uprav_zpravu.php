<?php
$zprava = Select::getInstance()->getZprava($_GET['id']);
if($zprava['ID_ODESILATELE'] == Authentication::getInstance()->getId()) :

    if(isset($_POST['upravit_zpravu'])){
        $text = trim($_POST['text']);
        if (!empty($text)) {
            Update::getInstance()->updateZprava($_GET['id'], $text);
            exit(header('Location: ' . BASE_URL . "?page=chat&id=" . $zprava['ID_KONTAKT']));
        }
    }
?>
<div class="card" style="margin-top: 15px;" id="dialog">
    <div class="card-title">
        <h3>Úprava zprávy</h3>
    </div>
    <div class="card-body">
        <form method="post">
            <div class="form-group">
                <label>Text komentáře:</label>
                <textarea style="margin-left: 17px;" placeholder="Text komentáře" style="resize: none;" rows="5" name="text"><?php echo $zprava['TEXT']; ?></textarea>
            </div>
            <div class="form-submit">
                <input type="submit" name="upravit_zpravu" value="Upravit zprávu">
            </div>
        </form>
    </div>
</div>
<?php
else:
    exit(header('Location: ' . BASE_URL . "?page=contacts"));
endif;
?>
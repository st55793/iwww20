<?php 
if (!Authentication::getInstance()->hasIdentity()) {
    exit(header('Location:' . BASE_URL . '?page=materialy'));
}

if (!isset($_GET['id_kviz'])) {
    exit(header('Location:' . BASE_URL . '?page=materialy'));
}

$id_kviz = $_GET['id_kviz'];

$kviz = Select::getInstance()->getKviz($id_kviz);
$pocet_pokusu = Select::getInstance()->getPocetPokusu($id_kviz)[0]['POCET'];
$pocet_otazek = Select::getInstance()->getPocetOtazek($id_kviz)[0]['POCET'];
?>



<div class="card">
    <div class="card-title">
        <h2>Vstup do kvízu</h2>
    </div>
    <div class="card-body" style="width: 100%;">
        <table class="profile" style="width: auto;">
            <tr>
                <th style="width: 35%;">Počet pokusů:</th>
                <th style="width: 50%;"><?php echo $kviz['POCET_POKUSU'] . " Využité pokusy: " . $pocet_pokusu; ?></th>
            </tr>
            <tr>
                <th>Název Kvízu:</th>
                <th><?php echo $kviz['NAZEV']; ?></th>
            </tr>
            <tr>
                <th>Popis kvízu:</th>
                <th><?php echo $kviz['POPIS']; ?></th>
            </tr>
            <tr>
                <th>Počet otázek:</th>
                <th><?php echo $pocet_otazek; ?></th>
            </tr>
            <tr>
                <th>Maximum bodů:</th>
                <th><?php echo $kviz['POCET_BODU']; ?></th>
            </tr>
            <tr>
                <th>Výsledky:</th>
                <th><a href="?page=vysledky">Výsledky</a></th>
            </tr>
        </table>
    </div>
    <?php
        if($pocet_pokusu < $kviz['POCET_POKUSU']){
    ?>
    <div class="card-body">
        <a onclick='return confirmVstup();' class="button_kviz" href="?page=zobraz_kviz&id_kviz=<?php echo $id_kviz;?>">Zahájit kvíz</a>
    </div>
    <?php
        }else{
            echo '<span class="error-msg">Už nemáš žádné pokusy</span>';
        }
    ?>
</div>

<script>
    function confirmVstup() {
        return confirm("Opravdu chcete zahájit kvíz ?\n");
    }
</script>
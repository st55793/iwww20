<?php
if (!Authentication::getInstance()->hasIdentity() || !isset($_GET['id_kviz'])) {
    exit(header('Location:' . BASE_URL . '?page=materialy'));
}
$kviz = Select::getInstance()->getKviz($_GET['id_kviz']);
$otazky = Select::getInstance()->getOtazky($_GET['id_kviz']);
$spravne = array();
$pocet_pokusu = Select::getInstance()->getPocetPokusu($_GET['id_kviz'])[0]['POCET'];

if (isset($_POST['vyhodnotit'])) {
    $body = 0;
    $o = $_POST['odpovedi'];
    $pom = 0;
    $pocet_spravne = 0;
    $pocet_spatne = 0;
    $pocet_castecne = 0;

    foreach ($o as $item) {
        $otazka = $otazky[$pom];
        if ($otazka['ID_TYP'] == 1) { // jedna
            if (!empty($item['odpoved'])) {
                $odpoved = Select::getInstance()->getOdpoved($item['odpoved']);
                if ($odpoved['PRIZNAK'] == 1) {
                    array_push($spravne, 1);
                    $body += $otazka['BODY'];
                    $pocet_spravne++;
                } else {
                    array_push($spravne, 0);
                    $pocet_spatne++;
                }
            } else {
                $pocet_spatne++;
                array_push($spravne, 0);
            }
        } else if ($otazka['ID_TYP'] == 2) { // text 
            $odpoved = Select::getInstance()->getOdpovedi($otazka['ID_OTAZKA']);
            if (strtolower($odpoved[0]['ODPOVED']) == strtolower($item['odpoved'])) {
                array_push($spravne, 1);
                $body += $otazka['BODY'];
                $pocet_spravne++;
            } else {
                array_push($spravne, 0);
                $pocet_spatne++;
            }
        } else if ($otazka['ID_TYP'] == 3) { // více
            $plus = 0;
            $minus = 0;
            if (isset($item['odpoved'])) {
                foreach ($item['odpoved'] as $id) {
                    $odpoved = Select::getInstance()->getOdpoved($id);
                    if ($odpoved['PRIZNAK'] == 1) {
                        $plus++;
                    }else{
                       $minus++; 
                    }
                }
            }
            if ($minus > 0) {
                array_push($spravne, 0);
                $pocet_spatne++;
            }else if ($otazka['POCET_SPRAVNYCH'] == $plus) { // správně vše
                array_push($spravne, 1);
                $pocet_spravne++;
                $body += $otazka['BODY'] * $plus;
            } else if ($plus != 0 && $otazka['POCET_SPRAVNYCH'] > $plus) { // častečně správně
                $body += $otazka['BODY'] * $plus;
                array_push($spravne, 2);
                $pocet_castecne++;
            } else { // špatně vše nebo nic nevyplněno 
                array_push($spravne, 0);
                $pocet_spatne++;
            }
        }
        $pom++;
    }

    Insert::getInstance()->InsertVysledek($body, $pocet_spravne, $pocet_castecne, $pocet_spatne, $_GET['id_kviz']);
    echo '<span class="error-msg">Výsledek by zaznamenán !!!</span>';
    $pocet_pokusu++;
}

echo "<h3>Kvíz: " . $kviz['NAZEV'] . "</h3>";
echo "<p>Kvíz: " . $kviz['POPIS'] . "</p></br>";
if (isset($body)) {
    echo '<span class="error-msg">Získal jsi: ' . $body . ' bodů</span>';
    echo "</br>";
}
// 1. jedna správna 2. text 3. více správných

if ($pocet_pokusu < $kviz['POCET_POKUSU']) :
?>
    <form method="POST">
        <?php
        $i = 0;
        foreach ($otazky as $item) {
            $odpovedi = Select::getInstance()->getOdpovedi($item['ID_OTAZKA']);
            if (!empty($spravne)) {
                if ($spravne[$i] == 1) {
                    echo '<h4 style="color: green">' . $item['OTAZKA'] . ' (' . $item['BODY'] . ' body za správnou odpověd)</h4>';
                } else if ($spravne[$i] == 0) {
                    echo '<h4 style="color: red">' . $item['OTAZKA'] . ' (' . $item['BODY'] . ' body za správnou odpověd)</h4>';
                } else if ($spravne[$i] == 2) {
                    echo '<h4 style="color: orange">' . $item['OTAZKA'] . ' (' . $item['BODY'] . ' body za správnou odpověd)</h4>';
                }
            } else {
                echo '<h4>' . $item['OTAZKA'] . ' (' . $item['BODY'] . ' body za správnou odpověd)</h4>';
            }
            //jedna
            if ($item['ID_TYP'] == 1) {
                foreach ($odpovedi as $odpoved) {
                    if (isset($o) && !empty($o[$i]['odpoved']) && $odpoved['ID_ODPOVED'] == $o[$i]['odpoved']) {
                        echo '<input type="radio" checked="checked" name="odpovedi[' . $i . '][odpoved]" value="' . $odpoved['ID_ODPOVED'] . '"/> ' . $odpoved['ODPOVED'] . ' </br>';
                    } else {
                        echo '<input type="radio" name="odpovedi[' . $i . '][odpoved]" value="' . $odpoved['ID_ODPOVED'] . '"/> ' . $odpoved['ODPOVED'] . ' </br>';
                    }
                }
            } else if ($item['ID_TYP'] == 2) { // text
                if (isset($o) && !empty($o[$i]['odpoved'])) {
                    echo 'Zadej odpověď: <input type="text" name="odpovedi[' . $i . '][odpoved]" value="' . $o[$i]['odpoved'] . '"/> </br>';
                } else {
                    echo 'Zadej odpověď: <input type="text" name="odpovedi[' . $i . '][odpoved]"/> </br>';
                }
            } else if ($item['ID_TYP'] == 3) { // více
                foreach ($odpovedi as $odpoved) {
                    if (isset($o) && !empty(!empty($o[$i]['odpoved'])) && in_array($odpoved['ID_ODPOVED'], $o[$i]['odpoved'])) {
                        echo '<input type="checkbox" checked="checked" name="odpovedi[' . $i . '][odpoved][]" value="' . $odpoved['ID_ODPOVED'] . '"/> ' . $odpoved['ODPOVED'] . ' </br>';
                    } else {
                        echo '<input type="checkbox" name="odpovedi[' . $i . '][odpoved][]" value="' . $odpoved['ID_ODPOVED'] . '"/> ' . $odpoved['ODPOVED'] . ' </br>';
                    }
                }
            }
            echo "</br>";
            echo '<input type="hidden" name="odpovedi[' . $i . '][id]" value="' . $item['ID_OTAZKA'] . '"/>';
            $i++;
        }
        ?>
        <div class="form-submit">
            <input type="submit" name="vyhodnotit" value="Vyhodnotit kvíz">
        </div>
    </form>
<?php else :
    echo '<span class="error-msg">Už nemáš žádné pokusy</span>';
endif;
?>
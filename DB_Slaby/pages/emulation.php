<?php if (Authentication::getInstance()->isAdmin() == false){
    exit(header('Location: ' . BASE_URL . "?page=profile"));
}
if (isset($_POST['submit'])) {
    var_dump($_POST['user']);
    Authentication::getInstance()->emulation($_POST['user']);
    exit(header('Location: ' . BASE_URL . '?page=profile'));
}
?>
<h3>Emulace uživatel</h3>
<form method="post">
    <p>
        <select name="user">
            <option value="">&nbsp;</option>
            <?php
            $results = Select::getInstance()->getAllUsers();
            echo "<option selected value='{$results["ID_UZIVATELE"][0]}'>{$results["JMENO"][0]} {$results["PRIJMENI"][0]}</option>\n";
            for ($i = 1; $i < count($results['LOGIN']); $i++) {
                echo "<option value='{$results["ID_UZIVATELE"][$i]}'>{$results["JMENO"][$i]} {$results["PRIJMENI"][$i]}</option>\n";
            }
            ?>
        </select>
    </p>
    <p>
        <input style="margin-top: 10px;" type="submit" name="submit" value="Emulovat" />
    </p>
</form>
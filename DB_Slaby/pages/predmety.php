<?php if (Authentication::getInstance()->isMentor() || Authentication::getInstance()->isAdmin()) :
    $predmety = Select::getInstance()->getPredmety();

    if (isset($_POST['pridat'])) {
        $nazev = $_POST['nazev'];
        $popis = $_POST['popis'];
        if (!empty(trim($nazev)) || !empty(trim($popis))) {
            Insert::getInstance()->InsertPredmet($nazev, $popis);
            exit(header('Location:' . BASE_URL . '?page=predmety'));
        }
    }

    if(isset($_GET['smaz']) && isset($_GET['id'])){
        Delete::getInstance()->deletePredmet($_GET['id']);
        exit(header('Location:' . BASE_URL . '?page=predmety'));
    }
?>



    <div class="card">
        <div class="card-title">
            <h2>Předměty</h2>
        </div>
        <div class="card-body" style="width: 100%;">
            <table class="predmety">
                <tr style="border-bottom: 1px black;">
                <?php
                    echo "<th class='th_predmety' style='width: 20%;'>Název předmětu</th>";
                    echo "<th class='th_predmety' style='width: 25%;'>Popis předmětu</th>";
                    echo "<th class='th_predmety'>Garant předmětu</th>";
                    echo "<th class='th_predmety'>Přednášející</th>";
                    if(Authentication::getInstance()->isMentor() || Authentication::getInstance()->isAdmin()){
                        echo "<th class='th_predmety'>Úprava</th>";
                        echo "<th class='th_predmety'>Mazání</th>";
                    }
                    ?>
                </tr>
                <?php
                $kontrola = array();
                foreach ($predmety as $item) {
                    $isOk = true;
                    $id = $item['ID_PREDMET'];
                    $nazev = $item['NAZEV'];
                    $popis = $item['POPIS'];
                    $garant = $item['GARANT'];
                    $prednasejici = $item['PREDNASEJICI'];

                    echo "<tr>";
                        echo "<td class='th_predmety'>" . $nazev . "</td>";
                        echo "<td class='th_predmety'>" . $popis . "</td>";
                        echo "<td class='th_predmety'>" . $garant . "</td>";
                        echo "<td class='th_predmety'>" . $prednasejici . "</td>";
                        if( Authentication::getInstance()->isAdmin() || Select::getInstance()->getKontrola($id)){
                            echo "<td class='th_predmety'><a href='" . BASE_URL . "?page=predmet&id=" . $id . "'>Upravit</a></td>";
                            echo "<td class='th_predmety'><a onclick='return confirmDelete();' href='" . BASE_URL . "?page=predmety&id=" . $id . "&smaz=1'>Smazat</a></td>";
                        }else{
                            echo "<td class='th_predmety'>Nemáš práva</td>"; 
                        }
                    echo "</tr>";           
                }
                ?>
            </table>
            <?php if (Authentication::getInstance()->isAdmin()) : ?>
                <button onclick="ShowDialog()">Přidat předmět</button>
                <div class="card" style="display: none; margin-top: 15px;" id="dialog">
                    <div class="card-title">
                        <h2>Přidání předmětu</h2>
                    </div>
                    <div class="card-body">
                        <form method="post">
                            <div class="form-group">
                                <label>Název předmětu:</label>
                                <input type="text" name="nazev" placeholder="Název předmětu">
                            </div>
                            <div class="form-group">
                                <label>Popis předmětu:</label>
                                <textarea style="margin-left: 75;" name="popis" cols="35" rows="5" placeholder="Zadej popis předmětu"></textarea>
                            </div>
                            <div class="form-submit">
                                <input type="submit" name="pridat" value="Přidat předmět">
                            </div>
                        </form>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
<?php endif; ?>
<script>
    function confirmDelete() {
        return confirm("Opravdu chcete Předmět smazat?\n");
    }
</script>
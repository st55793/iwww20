<header>
    <div id="header">
        <a href="<?= BASE_URL ?>">
            <img id="header-img" src="./images/logo.png" alt="logo">
        </a>
        <span id="header-title">ELSA</span>
    </div>
    <nav>
        <a href="<?= BASE_URL ?>">Domů</a>
        <?php if (Authentication::getInstance()->hasIdentity()) :
            if (Authentication::getInstance()->isAdmin()) {
        ?>
                <a href="<?= BASE_URL . "?page=sprava_uzivatelu" ?>">Správa uživatelů</a>
            <?php
            }
            if (Authentication::getInstance()->isMentor() || Authentication::getInstance()->isAdmin()) {
            ?>
            <a href="<?= BASE_URL . "?page=predmety" ?>">Předměty</a>
            <?php
            }
            ?>
            <a href="<?= BASE_URL . "?page=materialy" ?>">Studijní materiály</a>
            <a href="<?= BASE_URL . "?page=profile" ?>">Profil</a>
            <a href="<?= BASE_URL . "?page=logout" ?>">Logout</a>
        <?php else : ?>
            <a href="<?= BASE_URL . "?page=login" ?>">Login</a>
            <a href="<?= BASE_URL . "?page=registration" ?>">Registrace</a>
        <?php endif; ?>
    </nav>
    <a href="#" class="navbar-icon" onclick="return showMenu()">
        <i class="fa fa-bars"></i>
    </a>
</header>
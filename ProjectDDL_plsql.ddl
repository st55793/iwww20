
--mazani stareho obrazku - Při přidání nového profil obrázku se smaže starý
create or replace TRIGGER obrazek_trigger
BEFORE INSERT ON profilovy_obrazek
FOR EACH ROW
BEGIN
DELETE FROM profilovy_obrazek WHERE UZIVATELE_ID_UZIVATELE = :NEW.UZIVATELE_ID_UZIVATELE;
END;

/

--ochrana proti duplicitě - aby jeden cvicicí nebyl u jednohó předmetu víckrát než 1x
create or replace TRIGGER opravneni_trigger
BEFORE INSERT ON OPRAVNENI
FOR EACH ROW
BEGIN
    if :NEW.ROLE_ID_ROLE = 3 then
        DELETE FROM OPRAVNENI WHERE ROLE_ID_ROLE = 3 AND PREDMET_ID_PREDMET = :NEW.PREDMET_ID_PREDMET;
    else
        DELETE FROM OPRAVNENI WHERE ROLE_ID_ROLE = :NEW.ROLE_ID_ROLE AND UZIVATELE_ID_UZIVATELE = :NEW.UZIVATELE_ID_UZIVATELE AND PREDMET_ID_PREDMET = :NEW.PREDMET_ID_PREDMET;
    end if;
END;

/
--mazaní starého souboru - Při přidání nového souboru k mat. se smaže starý
create or replace TRIGGER soubor_trigger
BEFORE INSERT ON material_soubor
FOR EACH ROW
BEGIN
DELETE FROM material_soubor WHERE id_material = :NEW.id_material;
END;

/
--ochrana proti duplicitě
create or replace TRIGGER mat_kat_trigger
BEFORE INSERT ON material_kategorie
FOR EACH ROW
BEGIN
    DELETE FROM material_kategorie WHERE id_material = :NEW.id_material AND id_kategorie = :NEW.id_kategorie;
END;

/
--Odeslání mailu po registraci
create or replace TRIGGER REGISTRATION_TRIGGER
AFTER INSERT ON uzivatele
FOR EACH ROW
BEGIN
    INSERT INTO MAIL VALUES (MAIL_SEQ.nextval, 'info@ELSA.cz', :NEW.email, 'Vítej v ELSE', 'Vítej ve studijním portálu ELSA mužeš si zde prohlížet stud. materiály a vyplňovat kvízy', CURRENT_TIMESTAMP);
END;

/

--Odeslání mailu po změně
create or replace TRIGGER UPDATE_TRIGGER
AFTER UPDATE ON uzivatele
FOR EACH ROW 
BEGIN
    if :NEW.heslo <> :OLD.heslo then
        INSERT INTO MAIL VALUES (MAIL_SEQ.nextval, 'info@ELSA.cz', :NEW.email, 'Byl jsi to ty ?', 
        'Zdravím, někdo nyní změnil heslo na tvém účtu. Pokud jsi to nebyl ty prosím kontaktuj podporu, popř. si změň co nejrychleji heslo', CURRENT_TIMESTAMP);
    end if;
    if :NEW.email <> :OLD.email then
        INSERT INTO MAIL VALUES (MAIL_SEQ.nextval, 'info@ELSA.cz', :NEW.email, 'Byl jsi to ty ?', 
        'Zdravím, někdo nyní změnil email na tvém účtu. Pokud jsi to nebyl ty prosím kontaktuj podporu, popř. si změň co nejrychleji heslo', CURRENT_TIMESTAMP);
    end if;
END;


/

--funkce s návratovou hodnotou bud garanta nebo prednasejicích
CREATE OR REPLACE FUNCTION DEJUCITELE
(id_p IN NUMBER, id_r IN NUMBER) RETURN VARCHAR2 AS

v_vysledek VARCHAR2(255);

begin
    for x in (
        select * from P_UCITELE where id_predmet = id_p AND id_role = id_r
    ) 
    loop
        if id_r = 3 then
            v_vysledek := v_vysledek || x.prijmeni;
        else
            if v_vysledek IS NULL THEN
            v_vysledek := v_vysledek || x.prijmeni;
            else
            v_vysledek := v_vysledek ||', ' || x.prijmeni;
            end if;
        end if;
    end loop;
    
    IF v_vysledek IS NULL THEN
        v_vysledek := 'Žádní Přednášející';
    END IF;
    
    RETURN v_vysledek;
end DEJUCITELE;

/

--funkce s návratovou hodnotou pocet celkových bodů za kvíz
CREATE OR REPLACE FUNCTION DEJPOCETBODU
(id_k IN NUMBER) RETURN NUMBER AS

v_vysledek NUMBER(3);

begin
    for x in (
        SELECT * FROM P_OTAZKY WHERE ID_KVIZ = id_k
    ) 
    loop
        v_vysledek := v_vysledek + x.BODY * x.POCET_SPRAVNYCH;
    end loop;
    
    RETURN v_vysledek;

end DEJPOCETBODU;

/

--kontrola jestli nejsou stringy prázdné 
create or replace FUNCTION validuj_registraci(v_nick IN VARCHAR2, v_heslo IN VARCHAR2, v_jmeno IN VARCHAR2, v_prijmeni IN VARCHAR2, v_email IN VARCHAR2)
RETURN NUMBER
AS
BEGIN
IF (v_nick = '') THEN
RETURN 0;
END IF;
IF (v_heslo = '') THEN
RETURN 0;
END IF;
IF (v_jmeno = '') THEN
RETURN 0;
END IF;
IF (v_prijmeni = '') THEN
RETURN 0;
END IF;
IF (v_email = '') THEN
RETURN 0;
END IF;
RETURN 1;
END;

/

--vrací max. počet bodů v kvízu
CREATE OR REPLACE FUNCTION DEJPOCETBODU
(id_k IN NUMBER) 
RETURN NUMBER 
AS
Pocetbodu NUMBER;
begin
PocetBodu := 0;
    for x in (
        SELECT * FROM P_OTAZKY WHERE ID_KVIZ = id_k
    ) 
    loop
        Pocetbodu := Pocetbodu+x.BODY*x.POCET_SPRAVNYCH;
    end loop;
    
    RETURN Pocetbodu;

end DEJPOCETBODU;

/
--kontrola validace dat a nasledný insert
create or replace PROCEDURE registruj
(v_nick IN VARCHAR2, v_heslo IN VARCHAR2, v_jmeno IN VARCHAR2, v_prijmeni IN VARCHAR2, v_role IN NUMBER, v_email IN VARCHAR2)
IS
is_valid NUMBER;
BEGIN
SELECT validuj_registraci(v_nick, v_heslo, v_jmeno, v_prijmeni, v_email) INTO is_valid FROM DUAL;
IF (is_valid = 1) THEN
INSERT INTO uzivatele VALUES (UZIVATEL_SEQ.NEXTVAL, INITCAP(v_jmeno), INITCAP(v_prijmeni), v_nick, v_heslo, v_role, v_email);
END IF;
END;

/

--Mazání kontaktu -> nejdříve se smažou zprávy a až poté kontakt samotný
create or replace PROCEDURE SMAZ_KONTAKT
(v_kontakt IN NUMBER)
IS
BEGIN
DELETE FROM zprava WHERE id_kontakt = v_kontakt;
DELETE FROM kontakt WHERE id_kontakt = v_kontakt;
END SMAZ_KONTAKT;

/

--Mazání otázky -> nejdříve se smažou odpovedi a až poté otázka
CREATE OR REPLACE PROCEDURE SMAZ_OTAZKU
(id_o IN NUMBER)
IS
begin
    delete from ODPOVED where id_otazka = id_o;
    delete from OTAZKA where id_otazka = id_o;
end SMAZ_OTAZKU;

/

--Mazání kvízu -> nejdříve se smažou otázky a až poté kvíz
create or replace PROCEDURE SMAZ_KVIZ
(id_k IN NUMBER)
IS
begin
    for x in (
        SELECT * FROM P_OTAZKY WHERE ID_KVIZ = id_k
    ) 
    loop
        BEGIN SMAZ_OTAZKU(x.id_otazka); END;
    end loop;
    delete from vysledky where id_kviz = id_k;
    delete from kviz where id_kviz = id_k;
end SMAZ_KVIZ;

/

--Mazání studijního mteriálu -> nejdříve se smažou kategorie, komentaře, soubor, poté kvízy a nakonec studeijní materiál
CREATE OR REPLACE PROCEDURE SMAZ_MATERIAL
(id_m IN NUMBER)
IS
begin
    DELETE FROM MATERIAL_KATEGORIE WHERE ID_MATERIAL = id_m;
    DELETE FROM KOMENTARE WHERE ID_MATERIAL = id_m;
    DELETE FROM MATERIAL_SOUBOR WHERE ID_MATERIAL = id_m;

    for x in (
        SELECT * FROM P_KVIZY WHERE ID_MATERIAL = id_m
    ) 
    loop
        BEGIN SMAZ_KVIZ(x.id_kviz); END;
    end loop;

    DELETE FROM STUDIJNI_MATERIAL WHERE id_material = id_m;

end SMAZ_MATERIAL;

/ 

--Mazání předmětu > nejdříve se smažou cvičící a garant poté studijní materiály a nakonec predmet
CREATE OR REPLACE PROCEDURE SMAZ_PREDMET
(id_p IN NUMBER)
IS
begin
    DELETE FROM OPRAVNENI WHERE PREDMET_ID_PREDMET = id_p;
    for x in (
        SELECT * FROM STUDIJNI_MATERIAL WHERE ID_PREDMET = id_p
    ) 
    loop
        BEGIN SMAZ_MATERIAL(x.id_material); END;
    end loop;
    DELETE FROM PREDMET WHERE ID_PREDMET = id_p;
end SMAZ_PREDMET;


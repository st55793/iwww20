--DB

CREATE TABLE KATEGORIE 
    ( 
     id_kategorie INTEGER  NOT NULL , 
     nazev        VARCHAR2 (150)  NOT NULL , 
     popis        CLOB 
    ) 
;

ALTER TABLE KATEGORIE 
    ADD CONSTRAINT KATEGORIE_PK PRIMARY KEY ( id_kategorie ) ;

CREATE TABLE KOMENTARE 
    ( 
     id_komentar  INTEGER  NOT NULL , 
     text         CLOB  NOT NULL , 
     id_material  INTEGER  NOT NULL , 
     id_uzivatele INTEGER  NOT NULL 
    ) 
;

ALTER TABLE KOMENTARE 
    ADD CONSTRAINT KOMENTARE_PK PRIMARY KEY ( id_komentar ) ;

CREATE TABLE KONTAKT 
    ( 
     ID_KONTAKT     INTEGER  NOT NULL , 
     id_uzivatele_1 INTEGER  NOT NULL , 
     id_uzivatele_2 INTEGER  NOT NULL 
    ) 
;

ALTER TABLE KONTAKT 
    ADD CONSTRAINT KONTAKT_PK PRIMARY KEY ( ID_KONTAKT ) ;

CREATE TABLE KVIZ 
    ( 
     id_kviz         INTEGER  NOT NULL , 
     nazev           VARCHAR2 (150)  NOT NULL , 
     popis           CLOB  NOT NULL , 
     id_material     INTEGER  NOT NULL , 
     datum_vytvoreni TIMESTAMP WITH LOCAL TIME ZONE  NOT NULL , 
     pocet_pokusu    INTEGER  NOT NULL 
    ) 
;

ALTER TABLE KVIZ 
    ADD CONSTRAINT KVIZ_PK PRIMARY KEY ( id_kviz ) ;

CREATE TABLE MAIL 
    ( 
     id_mail    INTEGER  NOT NULL , 
     odesilatel VARCHAR2 (150)  NOT NULL , 
     prijemce   VARCHAR2 (150)  NOT NULL , 
     predmet    VARCHAR2 (250)  NOT NULL , 
     text       CLOB  NOT NULL , 
     datum      TIMESTAMP WITH LOCAL TIME ZONE  NOT NULL 
    ) 
;

ALTER TABLE MAIL 
    ADD CONSTRAINT MAIL_PK PRIMARY KEY ( id_mail ) ;

CREATE TABLE material_kategorie 
    ( 
     id_material  INTEGER  NOT NULL , 
     id_kategorie INTEGER  NOT NULL 
    ) 
;

ALTER TABLE material_kategorie 
    ADD CONSTRAINT material_kategorie_PK PRIMARY KEY ( id_material, id_kategorie ) ;

CREATE TABLE MATERIAL_SOUBOR 
    ( 
     id_soubor   INTEGER  NOT NULL , 
     soubor      BLOB  NOT NULL , 
     nazev       VARCHAR2 (60)  NOT NULL , 
     typ         VARCHAR2 (30)  NOT NULL , 
     datum       TIMESTAMP WITH LOCAL TIME ZONE  NOT NULL , 
     id_material INTEGER  NOT NULL 
    ) 
;

ALTER TABLE MATERIAL_SOUBOR 
    ADD CONSTRAINT MATERIAL_SOUBOR_PK PRIMARY KEY ( id_soubor ) ;

CREATE TABLE NEVHODNE_SLOVA 
    ( 
     id_slova INTEGER  NOT NULL , 
     slovo    VARCHAR2 (40)  NOT NULL 
    ) 
;

ALTER TABLE NEVHODNE_SLOVA 
    ADD CONSTRAINT NEVHODNE_SLOVA_PK PRIMARY KEY ( id_slova ) ;

CREATE TABLE ODPOVED 
    ( 
     id_odpoved INTEGER  NOT NULL , 
     odpoved    CLOB  NOT NULL , 
     priznak    INTEGER  NOT NULL , 
     id_otazka  INTEGER  NOT NULL 
    ) 
;

ALTER TABLE ODPOVED 
    ADD CONSTRAINT ODPOVED_PK PRIMARY KEY ( id_odpoved ) ;

CREATE TABLE OPRAVNENI 
    ( 
     id_opravneni           INTEGER  NOT NULL , 
     ROLE_id_role           INTEGER  NOT NULL , 
     UZIVATELE_id_uzivatele INTEGER  NOT NULL , 
     PREDMET_id_predmet     INTEGER  NOT NULL 
    ) 
;

ALTER TABLE OPRAVNENI 
    ADD CONSTRAINT OPRAVNENI_PK PRIMARY KEY ( id_opravneni ) ;

CREATE TABLE OTAZKA 
    ( 
     id_otazka       INTEGER  NOT NULL , 
     otazka          CLOB  NOT NULL , 
     body            INTEGER  NOT NULL , 
     id_kviz         INTEGER  NOT NULL , 
     id_typ          INTEGER  NOT NULL , 
     pocet_spravnych INTEGER 
    ) 
;

ALTER TABLE OTAZKA 
    ADD CONSTRAINT OTAZKA_PK PRIMARY KEY ( id_otazka ) ;

CREATE TABLE PREDMET 
    ( 
     id_predmet INTEGER  NOT NULL , 
     nazev      VARCHAR2 (150)  NOT NULL , 
     popis      CLOB  NOT NULL 
    ) 
;

ALTER TABLE PREDMET 
    ADD CONSTRAINT PREDMET_PK PRIMARY KEY ( id_predmet ) ;

CREATE TABLE PROFILOVY_OBRAZEK 
    ( 
     ID_profilovy_obrazek   INTEGER  NOT NULL , 
     obrazek                BLOB  NOT NULL , 
     typ                    VARCHAR2 (35)  NOT NULL , 
     datum                  TIMESTAMP WITH LOCAL TIME ZONE  NOT NULL , 
     UZIVATELE_id_uzivatele INTEGER 
    ) 
;

ALTER TABLE PROFILOVY_OBRAZEK 
    ADD CONSTRAINT PROFILOVY_OBRAZEK_PK PRIMARY KEY ( ID_profilovy_obrazek ) ;

CREATE TABLE ROLE 
    ( 
     id_role INTEGER  NOT NULL , 
     nazev   VARCHAR2 (70) 
    ) 
;

ALTER TABLE ROLE 
    ADD CONSTRAINT ROLE_PK PRIMARY KEY ( id_role ) ;

CREATE TABLE STUDIJNI_MATERIAL 
    ( 
     id_material     INTEGER  NOT NULL , 
     nazev           VARCHAR2 (250)  NOT NULL , 
     popis           CLOB  NOT NULL , 
     datum_vytvoreni TIMESTAMP WITH LOCAL TIME ZONE  NOT NULL , 
     datum_zmeny     TIMESTAMP WITH LOCAL TIME ZONE , 
     id_predmet      INTEGER  NOT NULL , 
     id_uzivatele    INTEGER  NOT NULL 
    ) 
;

ALTER TABLE STUDIJNI_MATERIAL 
    ADD CONSTRAINT STUDIJNI_MATERIAL_PK PRIMARY KEY ( id_material ) ;

CREATE TABLE TYP_OTAZKY 
    ( 
     id_typ INTEGER  NOT NULL , 
     nazev  VARCHAR2 (150)  NOT NULL , 
     popis  CLOB 
    ) 
;

ALTER TABLE TYP_OTAZKY 
    ADD CONSTRAINT TYP_OTAZKY_PK PRIMARY KEY ( id_typ ) ;

CREATE TABLE UZIVATELE 
    ( 
     id_uzivatele INTEGER  NOT NULL , 
     jmeno        VARCHAR2 (150)  NOT NULL , 
     prijmeni     VARCHAR2 (90)  NOT NULL , 
     login        VARCHAR2 (35)  NOT NULL , 
     heslo        VARCHAR2 (250)  NOT NULL , 
     ROLE_id_role INTEGER  NOT NULL , 
     email        VARCHAR2 (100)  NOT NULL 
    ) 
;

ALTER TABLE UZIVATELE 
    ADD CONSTRAINT UZIVATELE_PK PRIMARY KEY ( id_uzivatele ) ;

CREATE TABLE Vysledky 
    ( 
     id_vysledek  INTEGER  NOT NULL , 
     vysledek     INTEGER  NOT NULL , 
     spravne      INTEGER  NOT NULL , 
     castecne     INTEGER  NOT NULL , 
     spatne       INTEGER  NOT NULL , 
     id_uzivatele INTEGER  NOT NULL , 
     id_kviz      INTEGER  NOT NULL 
    ) 
;

ALTER TABLE Vysledky 
    ADD CONSTRAINT Vysledky_PK PRIMARY KEY ( id_vysledek ) ;

CREATE TABLE ZPRAVA 
    ( 
     ID_zprava      INTEGER  NOT NULL , 
     text           CLOB  NOT NULL , 
     datum          TIMESTAMP WITH LOCAL TIME ZONE  NOT NULL , 
     id_odesilatele INTEGER  NOT NULL , 
     id_prijemce    INTEGER  NOT NULL , 
     ID_KONTAKT     INTEGER  NOT NULL 
    ) 
;

ALTER TABLE ZPRAVA 
    ADD CONSTRAINT ZPRAVA_PK PRIMARY KEY ( ID_zprava ) ;

ALTER TABLE material_kategorie 
    ADD CONSTRAINT kategorie_MATERIAL_FK FOREIGN KEY 
    ( 
     id_material
    ) 
    REFERENCES STUDIJNI_MATERIAL 
    ( 
     id_material
    ) 
;

ALTER TABLE KOMENTARE 
    ADD CONSTRAINT KOMENTARE_STUDIJNI_MATERIAL_FK FOREIGN KEY 
    ( 
     id_material
    ) 
    REFERENCES STUDIJNI_MATERIAL 
    ( 
     id_material
    ) 
;

ALTER TABLE KOMENTARE 
    ADD CONSTRAINT KOMENTARE_UZIVATELE_FK FOREIGN KEY 
    ( 
     id_uzivatele
    ) 
    REFERENCES UZIVATELE 
    ( 
     id_uzivatele
    ) 
;

ALTER TABLE KONTAKT 
    ADD CONSTRAINT KONTAKT_UZIVATELE_FK FOREIGN KEY 
    ( 
     id_uzivatele_1
    ) 
    REFERENCES UZIVATELE 
    ( 
     id_uzivatele
    ) 
;

ALTER TABLE KONTAKT 
    ADD CONSTRAINT KONTAKT_UZIVATELE_FKv2 FOREIGN KEY 
    ( 
     id_uzivatele_2
    ) 
    REFERENCES UZIVATELE 
    ( 
     id_uzivatele
    ) 
;

ALTER TABLE KVIZ 
    ADD CONSTRAINT KVIZ_STUDIJNI_MATERIAL_FK FOREIGN KEY 
    ( 
     id_material
    ) 
    REFERENCES STUDIJNI_MATERIAL 
    ( 
     id_material
    ) 
;

ALTER TABLE material_kategorie 
    ADD CONSTRAINT material_KATEGORIRIE_FK FOREIGN KEY 
    ( 
     id_kategorie
    ) 
    REFERENCES KATEGORIE 
    ( 
     id_kategorie
    ) 
;

ALTER TABLE MATERIAL_SOUBOR 
    ADD CONSTRAINT MATERIAL_SOUBOR_MATERIAL_FK FOREIGN KEY 
    ( 
     id_material
    ) 
    REFERENCES STUDIJNI_MATERIAL 
    ( 
     id_material
    ) 
;

ALTER TABLE ODPOVED 
    ADD CONSTRAINT ODPOVED_OTAZKA_FK FOREIGN KEY 
    ( 
     id_otazka
    ) 
    REFERENCES OTAZKA 
    ( 
     id_otazka
    ) 
;

ALTER TABLE OPRAVNENI 
    ADD CONSTRAINT OPRAVNENI_PREDMET_FK FOREIGN KEY 
    ( 
     PREDMET_id_predmet
    ) 
    REFERENCES PREDMET 
    ( 
     id_predmet
    ) 
;

ALTER TABLE OPRAVNENI 
    ADD CONSTRAINT OPRAVNENI_ROLE_FK FOREIGN KEY 
    ( 
     ROLE_id_role
    ) 
    REFERENCES ROLE 
    ( 
     id_role
    ) 
;

ALTER TABLE OPRAVNENI 
    ADD CONSTRAINT OPRAVNENI_UZIVATELE_FK FOREIGN KEY 
    ( 
     UZIVATELE_id_uzivatele
    ) 
    REFERENCES UZIVATELE 
    ( 
     id_uzivatele
    ) 
;

ALTER TABLE OTAZKA 
    ADD CONSTRAINT OTAZKA_KVIZ_FK FOREIGN KEY 
    ( 
     id_kviz
    ) 
    REFERENCES KVIZ 
    ( 
     id_kviz
    ) 
;

ALTER TABLE OTAZKA 
    ADD CONSTRAINT OTAZKA_TYP_OTAZKY_FK FOREIGN KEY 
    ( 
     id_typ
    ) 
    REFERENCES TYP_OTAZKY 
    ( 
     id_typ
    ) 
;

ALTER TABLE PROFILOVY_OBRAZEK 
    ADD CONSTRAINT PROFILOVY_OBRAZEK_UZIVATELE_FK FOREIGN KEY 
    ( 
     UZIVATELE_id_uzivatele
    ) 
    REFERENCES UZIVATELE 
    ( 
     id_uzivatele
    ) 
;

ALTER TABLE STUDIJNI_MATERIAL 
    ADD CONSTRAINT STUDIJNI_MATERIAL_PREDMET_FK FOREIGN KEY 
    ( 
     id_predmet
    ) 
    REFERENCES PREDMET 
    ( 
     id_predmet
    ) 
;

ALTER TABLE STUDIJNI_MATERIAL 
    ADD CONSTRAINT STUDIJNI_MATERIAL_UZIVATELE_FK FOREIGN KEY 
    ( 
     id_uzivatele
    ) 
    REFERENCES UZIVATELE 
    ( 
     id_uzivatele
    ) 
;

ALTER TABLE UZIVATELE 
    ADD CONSTRAINT UZIVATELE_ROLE_FK FOREIGN KEY 
    ( 
     ROLE_id_role
    ) 
    REFERENCES ROLE 
    ( 
     id_role
    ) 
;

ALTER TABLE Vysledky 
    ADD CONSTRAINT Vysledky_KVIZ_FK FOREIGN KEY 
    ( 
     id_kviz
    ) 
    REFERENCES KVIZ 
    ( 
     id_kviz
    ) 
;

ALTER TABLE Vysledky 
    ADD CONSTRAINT Vysledky_UZIVATELE_FK FOREIGN KEY 
    ( 
     id_uzivatele
    ) 
    REFERENCES UZIVATELE 
    ( 
     id_uzivatele
    ) 
;

ALTER TABLE ZPRAVA 
    ADD CONSTRAINT ZPRAVA_KONTAKT_FK FOREIGN KEY 
    ( 
     ID_KONTAKT
    ) 
    REFERENCES KONTAKT 
    ( 
     ID_KONTAKT
    ) 
;

ALTER TABLE ZPRAVA 
    ADD CONSTRAINT ZPRAVA_UZIVATELE_FK FOREIGN KEY 
    ( 
     id_odesilatele
    ) 
    REFERENCES UZIVATELE 
    ( 
     id_uzivatele
    ) 
;

ALTER TABLE ZPRAVA 
    ADD CONSTRAINT ZPRAVA_UZIVATELE_FKv2 FOREIGN KEY 
    ( 
     id_prijemce
    ) 
    REFERENCES UZIVATELE 
    ( 
     id_uzivatele
    ) 
;


--Sequence
CREATE SEQUENCE Uzivatel_seq
    START WITH 100
    INCREMENT BY 1
    NOMAXVALUE
    NOCYCLE
    CACHE 20;

CREATE SEQUENCE Log_seq
    START WITH 100
    INCREMENT BY 1
    NOMAXVALUE
    NOCYCLE
    CACHE 20;

CREATE SEQUENCE PROFILOVY_OBRAZEK_SEQ
    START WITH 100
    INCREMENT BY 1
    NOMAXVALUE
    NOCYCLE
    CACHE 20;

CREATE SEQUENCE OPRAVNENI_SEQ
    START WITH 100
    INCREMENT BY 1
    NOMAXVALUE
    NOCYCLE
    CACHE 20;

CREATE SEQUENCE PREDMET_SEQ
    START WITH 100
    INCREMENT BY 1
    NOMAXVALUE
    NOCYCLE
    CACHE 20;

CREATE SEQUENCE KONTAKT_SEQ
    START WITH 100
    INCREMENT BY 1
    NOMAXVALUE
    NOCYCLE
    CACHE 20;

CREATE SEQUENCE ZPRAVA_SEQ
    START WITH 100
    INCREMENT BY 1
    NOMAXVALUE
    NOCYCLE
    CACHE 20;

CREATE SEQUENCE MATERIAL_SEQ
    START WITH 100
    INCREMENT BY 1
    NOMAXVALUE
    NOCYCLE
    CACHE 20;

CREATE SEQUENCE MATERIAL_SOUBOR_SEQ
    START WITH 100
    INCREMENT BY 1
    NOMAXVALUE
    NOCYCLE
    CACHE 20;

CREATE SEQUENCE KOMENTAR_SEQ
    START WITH 100
    INCREMENT BY 1
    NOMAXVALUE
    NOCYCLE
    CACHE 20;

CREATE SEQUENCE KVIZ_SEQ
    START WITH 100
    INCREMENT BY 1
    NOMAXVALUE
    NOCYCLE
    CACHE 20;

CREATE SEQUENCE OTAZKA_SEQ
    START WITH 100
    INCREMENT BY 1
    NOMAXVALUE
    NOCYCLE
    CACHE 20;

CREATE SEQUENCE ODPOVED_SEQ
    START WITH 100
    INCREMENT BY 1
    NOMAXVALUE
    NOCYCLE
    CACHE 20;

    
CREATE SEQUENCE SLOVA_SEQ
    START WITH 100
    INCREMENT BY 1
    NOMAXVALUE
    NOCYCLE
    CACHE 20;

CREATE SEQUENCE VYSLEDKY_SEQ
    START WITH 100
    INCREMENT BY 1
    NOMAXVALUE
    NOCYCLE
    CACHE 20;

CREATE SEQUENCE MAIL_SEQ
    START WITH 100
    INCREMENT BY 1
    NOMAXVALUE
    NOCYCLE
    CACHE 20;

--Pohledy 

Create OR replace view P_KOMENTARE AS
Select k.id_komentar, k.text, k.id_material, u.id_uzivatele, u.jmeno, u.prijmeni
from komentare k
JOIN uzivatele u ON u.id_uzivatele = k.id_uzivatele;

Create OR replace view P_KATEGORIE AS
Select "ID_KATEGORIE","NAZEV","POPIS" 
from kategorie k;

Create or replace view P_KONTAKTY AS
SELECT k.id_kontakt, k.id_uzivatele_1 as id_1,k.id_uzivatele_2 as id_2, u1.jmeno as jmeno_1, u1.prijmeni as prijmeni_1, u2.jmeno as jmeno_2, u2.prijmeni as prijmeni_2 
FROM kontakt k 
JOIN UZIVATELE u1 ON u1.id_uzivatele = k.id_uzivatele_1
JOIN UZIVATELE u2 ON u2.id_uzivatele = k.id_uzivatele_2;

Create OR replace view P_KVIZY AS
Select k.id_kviz, k.nazev, k.popis, k.id_material, k.datum_vytvoreni, k.pocet_pokusu, 
dejpocetbodu(id_kviz) AS pocet_bodu
from kviz k;

Create OR replace view P_MAT_KAT AS
Select "ID_MATERIAL","ID_KATEGORIE" 
from material_kategorie mk;

CREATE OR REPLACE VIEW P_MATERIALY AS
SELECT 
   s.id_material, s.nazev, s.popis, TO_CHAR(s.datum_vytvoreni, 'DD.MM.YYYY HH24:MI:SS') AS datum_vytvoreni, TO_CHAR(s.datum_zmeny, 'DD.MM.YYYY HH24:MI:SS') AS datum_zmeny, 
   s.id_predmet, p.nazev AS nazev_predmetu, u.prijmeni, u.id_uzivatele, f.soubor, f.nazev as nazev_souboru, f.typ as typ_souboru, 
   TO_CHAR(f.datum, 'DD.MM.YYYY HH24:MI:SS') as datum_soubor
FROM 
    studijni_material s
JOIN predmet p on p.id_predmet = s.id_predmet
JOIN uzivatele u on s.id_uzivatele = u.id_uzivatele
JOIN material_soubor f on f.id_material = s.id_material
ORDER BY nazev_predmetu ASC;

CREATE OR REPLACE VIEW P_NEVHODNA_SLOVA AS
SELECT * FROM NEVHODNA_SLOVA;

CREATE OR REPLACE VIEW P_ODPOVEDI AS
SELECT "ID_ODPOVED","ODPOVED","PRIZNAK","ID_OTAZKA" FROM ODPOVED;

Create OR replace view P_OTAZKY AS
Select id_otazka, otazka, body, id_kviz, o.id_typ, pocet_spravnych, t.nazev
from otazka o
JOIN TYP_OTAZKY t on t.id_typ = o.id_typ;

create or replace view P_PREDMETY AS 
select id_predmet, nazev, popis, dejucitele(id_predmet, 3) as garant, dejucitele(id_predmet, 4) as prednasejici from predmet;

Create or replace view P_ROLE AS 
SELECT "ID_ROLE","NAZEV" FROM role;

CREATE OR REPLACE VIEW P_TYPY AS
SELECT "ID_TYP","NAZEV","POPIS" FROM typ_otazky;

create or replace VIEW P_UCITELE as 
select prijmeni, ROLE.id_role, role.nazev, predmet.id_predmet, uzivatele.id_uzivatele, predmet.nazev AS nazev_predmetu from uzivatele 
join opravneni on uzivatele.id_uzivatele = opravneni.uzivatele_id_uzivatele
join role on opravneni.role_id_role = role.id_role
join predmet on opravneni.predmet_id_predmet = predmet.id_predmet;

Create or replace view P_UZIVATELE AS 
SELECT u.id_uzivatele ,u.login, u.jmeno, u.prijmeni, u.email, r.nazev as role, R.ID_ROLE 
FROM UZIVATELE u 
JOIN ROLE r ON u.role_id_role = r.id_role
ORDER BY u.jmeno ASC;

Create OR replace view P_ZPRAVY AS
Select z.id_kontakt, u.id_uzivatele AS id_odesilatele, uk.id_uzivatele AS id_prijemce, z.text, TO_CHAR(z.datum, 'DD.MM.YYYY HH24:MI:SS') AS datum, z.id_zprava, 
u.jmeno AS jmeno_odesilatele, uk.jmeno AS jmeno_prijemce from ZPRAVA z
JOIN uzivatele u ON u.id_uzivatele = z.id_odesilatele
Join uzivatele uk ON uk.id_uzivatele = z.id_prijemce
ORDER BY datum ASC;

CREATE OR REPLACE VIEW P_VYSLEDKY AS
select v.vysledek, k.nazev AS nazev_kvizu, v.id_uzivatele, v.id_kviz, v.id_vysledek, 
p.nazev AS nazev_predmetu, u.jmeno, u.prijmeni, p.id_predmet, v.spravne, v.spatne, 
v.castecne, DEJPOCETBODU(v.id_kviz) AS POCET_BODU
from vysledky v
JOIN uzivatele u ON u.id_uzivatele = v.id_uzivatele 
JOIN kviz k ON v.id_kviz = k.id_kviz 
JOIN studijni_material m ON k.id_material = m.id_material 
JOIN predmet p ON p.id_predmet = m.id_predmet
ORDER BY NAZEV_KVIZU, NAZEV_PREDMETU ASC